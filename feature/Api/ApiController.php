<?php

namespace Flood\Canal\Feature\Api;

use Flood\Canal\Controller\ApiBase;
use Flood\Captn\EventDispatcher;

class ApiController extends ApiBase {

    static $origin_allowed = [];
    static $header_allowed = [];
    static $header_expose = [];

    /**
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend, [
            'origin_allowed' => static::$origin_allowed,
            'header_allowed' => static::$header_allowed,
            'header_expose'  => static::$header_expose,
        ]);

        $allowed = EventDispatcher::trigger('feature.user.allowed', [
            'id'         => 'CONTROLLER_CALL',
            'value'      => 'api',
            'is_allowed' => false,
        ]);

        if(true !== $allowed['is_allowed']) {
            \Flood\Canal\Url\Url::addStatusHeader(403);
            $this->resp_data = 'Access to api route is not allowed.';
            $this->respondJson();
            exit();
        }
    }
}