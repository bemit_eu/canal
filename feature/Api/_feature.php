<?php

use \Flood\Captn\EventDispatcher;
use \Flood\Canal\Feature\Api\ApiController;

return static function($config = []) {
    /**
     * @param $frontend \Flood\Canal\Frontend
     *
     * @return \Flood\Canal\Feature\FileI
     */
    return static function($frontend) use ($config) {
        return new class($frontend, $config) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {
            public $config = [];

            public function __construct($frontend, $config) {
                parent::__construct($frontend);

                $this->config = $config;

                $this->name('api');

                EventDispatcher::on('core.init', [$this, 'init']);
            }

            public function init() {
                // todo: refine config setting for merging/overwriting header values

                if(isset($this->config['origin_allowed'])) {
                    ApiController::$origin_allowed = $this->config['origin_allowed'];
                }

                if(isset($this->config['header_allowed']) && !empty($this->config['header_allowed'])) {
                    ApiController::$header_allowed = $this->config['header_allowed'];
                } else {
                    ApiController::$header_allowed = [
                        'Content-Type',
                        'Accept',
                        'AUTHORIZATION',
                        'X-Requested-With',
                        'X_AUTH_TOKEN',
                        'X_AUTH_SIGNATURE',
                        'X_API_OPTION',
                        'remember-me',
                    ];
                }

                if(isset($this->config['header_expose']) && !empty($this->config['header_expose'])) {
                    ApiController::$header_expose = $this->config['header_expose'];
                } else {
                    ApiController::$header_expose = [
                        'Content-Range',
                    ];
                }
            }
        };
    };
};