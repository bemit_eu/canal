<?php

use \Flood\Captn\EventDispatcher;

/**
 * @param $frontend \Flood\Canal\Frontend
 *
 * @return \Flood\Canal\Feature\FileI
 */
return static function($frontend) {
    return new class($frontend) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {

        public function __construct($frontend) {
            parent::__construct($frontend);

            $this->name('cache');

            EventDispatcher::on('core.console.init', [$this, 'initConsole']);
        }

        public function initConsole(\Flood\Canal\Console $console) {
            $console::addScope('cache', static function() {
                return new Flood\Canal\Feature\Cache\Console\Cache();
            });

            return $console;
        }
    };
};