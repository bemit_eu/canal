<?php

namespace Flood\Canal\Feature\Cache\Console;

use Flood\Canal\Console;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class Cache
 *
 * @package   Flood\Canal\Console
 * @link      https://painttheweb.de/flood-canal/console-cli-mode
 * @author    Michael Becker
 * @copyright 2018 bemit UG (haftungsbeschraenkt)
 */
class Cache {
    /**
     * Could be called with `cache del <something>`, this will delete the cache for the something, here it is the `path` of the subfolder to be deleted
     *
     * @param string $path which cache path should be deleted, `*` deletes all subfolders
     */
    public function del($path) {
        $fs = new Filesystem;
        if(!Console::$silence) {
            \Flood\Canal\writeln('Cache deletion of path: ' . $path);
        }

        if('*' === $path || 'all' === $path) {
            if(file_exists(Console::$frontend->path_tmp)) {
                $fs->remove(Console::$frontend->path_tmp);
                if(!Console::$silence) {
                    \Flood\Canal\writeln('Deleted.');
                }
            } else if(Console::$debug) {
                \Flood\Canal\writeln('Could not delete path, doesn\'t exist.');
            } else if(!Console::$silence) {
                \Flood\Canal\writeln('Deletion failed.');
            }
        } else {
            if(file_exists(Console::$frontend->path_tmp . $path)) {
                $fs->remove(Console::$frontend->path_tmp . $path);
                if(!Console::$silence) {
                    \Flood\Canal\writeln('Deleted.');
                }
            } else if(Console::$debug) {
                \Flood\Canal\writeln('Could not delete path, doesn\'t exist.');
            } else if(!Console::$silence) {
                \Flood\Canal\writeln('Deletion failed.');
            }
        }
    }
}