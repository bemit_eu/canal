<?php

use \Flood\Captn\EventDispatcher;

return static function($config = []) {
    /**
     * @param $frontend \Flood\Canal\Frontend
     *
     * @return \Flood\Canal\Feature\FileI
     */
    return static function($frontend) use ($config) {
        return new class($frontend, $config) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {
            /**
             * @var \Twig_Loader_Filesystem
             */
            public $twig_loader = null;
            /**
             * @var \Twig_Environment
             */
            public $twig_env = null;
            public $config = [];

            public function __construct($frontend, $config) {
                parent::__construct($frontend);

                $this->config = $config;

                $this->name('twig');

                EventDispatcher::on('core.init', [$this, 'init']);
                EventDispatcher::on('view.render.file', [$this, 'templateFileRender']);
                EventDispatcher::on('view.render.string', [$this, 'templateStringRender']);
            }

            public function init() {
                $this->config['debug'] = $this->frontend->debug;

                $this->config['cache'] = $this->frontend->path_tmp . 'tpl';
                if(!is_array($this->frontend->path_view)) {
                    $this->frontend->path_view = [$this->frontend->path_view];
                }

                /**
                 * getting the first folder from the path_view array, this is the default view folder
                 */
                $tmp_arr = $this->frontend->path_view;
                $this->twig_loader = new \Twig_Loader_Filesystem(array_shift($tmp_arr));

                foreach($this->frontend->path_view as $key => $value) {
                    try {
                        if(is_string($key)) {
                            $this->twig_loader->addPath($key, $value);
                        } else {
                            $this->twig_loader->addPath($value);
                        }
                    } catch(\Twig_Error_Loader $e) {
                        echo $e->getMessage() . "\r\n";
                    }
                }

                $this->twig_env = new \Twig_Environment($this->twig_loader, $this->config);

                if($this->frontend->debug) {
                    // make dump() and more available
                    $this->twig_env->addExtension(new \Twig_Extension_Debug());
                }
            }

            /**
             * @param $payload
             *
             * @return string
             */
            public function templateFileRender($payload) {
                $tpl = $payload['tpl'];
                $data = $payload['data'];
                /**
                 * @var \Twig_TemplateWrapper $template_wrapper
                 */
                $template_wrapper = $this->twig_env->load($tpl);
                $payload['rendered'] = $this->render($template_wrapper, $data);
                return $payload;
            }

            /**
             * @param $payload
             *
             * @return string
             */
            public function templateStringRender($payload) {
                $tpl = $payload['tpl'];
                $data = $payload['data'];
                /**
                 * @var \Twig_Template $template_wrapper
                 */
                $template_wrapper = $this->twig_env->createTemplate($tpl);

                $payload['rendered'] = $this->render($template_wrapper, $data);

                return $payload;
            }

            /**
             * @param \Twig_TemplateWrapper|\Twig_Template $wrapper
             * @param array                                $data
             *
             * @return mixed
             */
            protected function render($wrapper, $data) {
                return $wrapper->render($data);
            }
        };
    };
};