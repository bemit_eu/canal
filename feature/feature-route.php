<?php

use Flood\Captn\EventDispatcher;

return static function($config = []) {
    /**
     * @param $frontend \Flood\Canal\Frontend
     *
     * @return \Flood\Canal\Feature\FileI
     */
    return static function($frontend) use ($config) {
        return new class($frontend, $config) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {

            protected $config = [];

            protected $route;

            public function __construct($frontend, $config) {
                parent::__construct($frontend);

                $this->config = $config;
                $this->route = new \Flood\Canal\Route\Routing($this->frontend->host, $this->frontend->ssl, $this->config['debug']);

                $this->name('route');

                if(isset($this->config['api_base'])) {
                    \Flood\Canal\Controller\ApiRoute::$path_base = $this->config['api_base'];
                }

                EventDispatcher::on('core.init', [$this, 'init']);
                EventDispatcher::on('core.route.match', [$this, 'routeMatch']);
            }

            public function init() {
                $this->frontend->route = &$this->route;
                $this->frontend->url_generator = &$this->route->url_generator;
            }

            public function routeMatch() {
                $this->route->addList($this->frontend->route_list);
                return $this->route->match();
            }
        };
    };
};