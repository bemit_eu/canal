<?php

namespace Flood\Canal\Feature\FileManager\Api;

use Flood\Canal\Feature\Api\ApiController;
use Flood\Canal\Feature\FileManager\FileManager;

class File extends ApiController {
    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }

    public function list($dir) {
        $this->resp_data = [];

        $filem = new FileManager();
        try {
            $this->resp_data = $filem->list($dir);
        } catch(\Exception $e) {
            $this->resp_data = ['error' => 'access-not-allowed'];
        }

        $this->respondJson();
    }

    /**
     * @throws \Exception
     */
    public function receiveFile($dir) {
        $this->resp_data = [];

        $data = false;
        if(filter_has_var(INPUT_POST, 'data')) {
            $data = filter_input(INPUT_POST, 'data', FILTER_UNSAFE_RAW);
        }

        if(isset($_FILES['file'])) {
            $file = $_FILES['file'];

            if($file['error']) {
                $this->resp_data = ['error' => 'generic-receiving-error'];
            } else if(!$file['tmp_name']) {
                $this->resp_data = ['error' => 'no-file-received'];
            } else {
                $filem = new FileManager();
                $done = $filem->handleFileUpload($file, $dir);
                if(true === $done) {
                    $this->resp_data = ['success' => $file['name']];
                } else if(-1 === $done) {
                    $this->resp_data = ['error' => 'file-already-exists'];
                } else {
                    $this->resp_data = ['error' => 'can-not-save'];
                }
            }
        } else {
            $this->resp_data = ['error' => true];
        }

        $this->respondJson();
    }

    public function renameFile($file) {
        $this->resp_data = [];

        $value = file_get_contents('php://input');
        $value = json_decode($value, true);

        if(isset($value['to'])) {
            $filem = new FileManager();
            try {
                if($filem->renameFile($file, $value['to'])) {
                    $this->resp_data = ['data' => true];
                } else {
                    $this->resp_data = ['error' => 'file-not-renamed'];
                }
            } catch(\Exception $e) {
                $this->resp_data = ['error' => 'access-not-allowed'];
            }
        } else {
            $this->resp_data = ['error' => 'rename-to-not-set'];
        }
        $this->respondJson();
    }

    public function deleteFile($file) {
        $this->resp_data = [];

        $filem = new FileManager();
        try {
            if($filem->deleteFile($file)) {
                $this->resp_data = ['data' => true];
            } else {
                $this->resp_data = ['error' => 'file-not-deleted'];
            }
        } catch(\Exception $e) {
            $this->resp_data = ['error' => 'access-not-allowed'];
        }
        $this->respondJson();
    }

    public function createDir($dir) {
        $this->resp_data = [];

        $filem = new FileManager();
        try {
            if($filem->createDir($dir)) {
                $this->resp_data = ['data' => true];
            } else {
                $this->resp_data = ['error' => 'dir-not-created'];
            }
        } catch(\Exception $e) {
            $this->resp_data = ['error' => 'access-not-allowed'];
        }

        $this->respondJson();
    }

    public function renameDir($dir) {
        $this->resp_data = [];

        $value = file_get_contents('php://input');
        $value = json_decode($value, true);

        if(isset($value['to'])) {
            $filem = new FileManager();
            try {
                if($filem->renameDir($dir, $value['to'])) {
                    $this->resp_data = ['data' => true];
                } else {
                    $this->resp_data = ['error' => 'dir-not-renamed'];
                }
            } catch(\Exception $e) {
                $this->resp_data = ['error' => 'access-not-allowed'];
            }
        } else {
            $this->resp_data = ['error' => 'rename-to-not-set'];
        }
        $this->respondJson();
    }

    public function deleteDir($dir) {
        $this->resp_data = [];

        $filem = new FileManager();
        try {
            if($filem->deleteDir($dir)) {
                $this->resp_data = ['data' => true];
            } else {
                $this->resp_data = ['error' => 'dir-not-deleted'];
            }
        } catch(\Exception $e) {
            if('dir-not-empty' === $e->getMessage()) {
                $this->resp_data = ['error' => 'dir-not-empty'];
            } else if('dir-not-in-root' === $e->getMessage()) {
                $this->resp_data = ['error' => 'access-not-allowed'];
            }
        }
        $this->respondJson();
    }
}