<?php

use \Flood\Captn\EventDispatcher;

/**
 * @param $frontend \Flood\Canal\Frontend
 *
 * @return \Flood\Canal\Feature\FileI
 */
return static function($frontend) {
    return new class($frontend) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {

        public function __construct($frontend) {
            parent::__construct($frontend);

            $this->name('file_manager');

            EventDispatcher::on('core.init', [$this, 'init']);
        }

        public function init() {
            $route = require __DIR__ . '/_routeApi.php';
            $route($this->frontend->route);

            \Flood\Canal\Feature\FileManager\FileManager::$upload_root = $this->frontend->path_data . 'out/upload';
        }
    };
};