<?php

use \Flood\Canal\Controller\ApiRoute;

/**
 * @param \Flood\Canal\Route\Routing $route
 */
return function($route) {
    ////
    //
    // File API
    //
    ////

    //
    // File

    (new ApiRoute('api-file-manager--add-file', '/files/add-one/{dir}', ['dir' => false], ['dir' => '.+']))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->receiveFile($frontend->match['dir']);
        });

    (new ApiRoute('api-file-manager--rename-file', '/files/rename-file/{file}', ['file' => false], ['file' => '.+']))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->renameFile($frontend->match['file']);
        });

    (new ApiRoute('api-file-manager--delete-file', '/files/delete-file/{file}', ['file' => false], ['file' => '.+']))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->deleteFile($frontend->match['file']);
        });

    //
    // Dir

    (new ApiRoute('api-file-manager--list', '/files/list/{dir}', ['dir' => false], ['dir' => '.+']))
        ->setRouter($route)
        ->get(static function(
            /** @param \Flood\Canal\Frontend $frontend */
            $frontend
        ) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->list($frontend->match['dir']);
        });

    (new ApiRoute('api-file-manager--create-dir', '/files/create-dir/{dir}', ['dir' => false], ['dir' => '.+']))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->createDir($frontend->match['dir']);
        });

    (new ApiRoute('api-file-manager--rename-dir', '/files/rename-dir/{dir}', ['dir' => false], ['dir' => '.+']))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->renameDir($frontend->match['dir']);
        });

    (new ApiRoute('api-file-manager--delete-dir', '/files/delete-dir/{dir}', ['dir' => false], ['dir' => '.+']))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\FileManager\Api\File($frontend);
            $controller->deleteDir($frontend->match['dir']);
        });
};