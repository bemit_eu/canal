<?php

namespace Flood\Canal\Feature\Content;

/**
 * Class Article
 *
 * @package Flood\Canal\Feature\Content
 *
 * @todo    : finish renaming "content" "content_file" to "mainText", "mainText_file" usw.
 */
class Article {

    /**
     * @var \Flood\Canal\Feature\Content\ArticleMeta
     */
    protected $meta;
    /**
     * @var \Flood\Canal\Feature\Content\DocTree
     */
    protected $tree;

    /**
     * @var string
     */
    protected $mainText_file = '';
    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var null|string
     */
    public $mainText_raw = null;
    /**
     * @var null}string
     */
    public $mainText_parsed = null;

    /**
     * @var string
     */
    public $id;

    /**
     * @var \Flood\Canal\Feature\Content\Index
     */
    public $index;

    /**
     * @var callable that received the raw content and processes the template language
     */
    protected $renderer_text = null;
    protected $renderer_file = null;
    /**
     * @var callable that receive the rendered template and will parse the content
     */
    protected $parser = null;

    /**
     * Content\Article constructor.
     *
     * @param                                    $id
     * @param                                    $path
     * @param                                    $file
     * @param \Flood\Canal\Feature\Content\Index $index
     *
     * @throws \Exception
     */
    public function __construct($id, $path, $file, &$index) {
        if(!empty($file)) {
            $this->path = $path . $file;
        } else {
            $this->path = $path . $id;
        }

        $this->id = $id;

        $this->index = &$index;

        $this->meta = new ArticleMeta($this->path, $this);
        $this->initTree();

        $this->mainText_file = $this->path . '.md';
    }

    protected function initTree() {
        if(true === $this->meta->get('_doc-tree')) {
            $this->tree = new DocTree($this->path, $this);
            $this->tree->provideRenderEnv(
                $this->renderer_text,
                $this->renderer_file,
                $this->parser
            );
        } else {
            $this->tree = false;
        }
    }

    /**
     * @param null $key
     *
     * @deprecated use `meta` instead
     *
     * @return array|mixed|null
     */
    public function get($key = null) {
        return $this->meta($key);
    }

    /**
     * Gets a value from the meta file
     *
     * @param $key
     *
     * @return array|null|mixed
     */
    public function meta($key = null) {
        return $this->meta->get($key);
    }

    /**
     * @return bool
     */
    public function hasTree() {
        if(false === $this->tree) {
            return false;
        }
        return true;
    }

    /**
     * For getting one fragment, ready to get rendered or an list of all fragments
     *
     * @param string $fragment_name when null returns list of all fragments
     * @param bool        $text          if the rendered should be a array with an entry for each block or everything in one text string
     *
     * @throws \Exception
     *
     * @return array|string
     */
    public function renderBlock($fragment_name, $text = true) {
        if($this->hasTree()) {
            return $this->tree->getRootBlock($fragment_name, $text);
        }
        if($text) {
            return '';
        }
        return [];
    }

    /**
     * Returns List
     *
     * @param null|string $fragment_name
     *
     * @return array
     */
    public function getDocTreeDefinition($fragment_name = null) {
        if($this->hasTree()) {
            return $this->tree->getDefinition($fragment_name);
        }

        return [];
    }

    /**
     * Creates a meta file for this article if needed
     *
     * @param $data
     *
     * @return bool
     */
    public function metaCreate($data) {
        if($this->meta->createFile($data)) {

            try {
                $this->meta->loadFile();
            } catch(\Exception $e) {
                error_log($e->getMessage());

                return false;
            }

            $this->initTree();

            return true;
        }

        return false;
    }

    /**
     * Creates a tree file for this article if needed
     *
     * @param $data
     *
     * @return bool
     */
    public function treeCreate($data) {
        if(false !== $this->tree) {
            if($this->tree->createFile($data)) {
                try {
                    $this->tree->loadFile();

                    return true;
                } catch(\Exception $e) {
                    error_log($e->getMessage());
                }
            }
        }

        return false;
    }

    /**
     * @param callable $renderer_text template language parsing, e.g. Twig Text
     * @param callable $renderer_file template file parsing, e.g. Twig File
     * @param callable $parser        content language parsing, e.g. MarkDown
     */
    public function provideRenderEnv($renderer_text, $renderer_file, $parser) {
        $this->renderer_text = $renderer_text;
        $this->renderer_file = $renderer_file;
        $this->parser = $parser;
    }

    /**
     * @param bool $raw
     *
     * @deprecated use getMainText instead
     *
     * @return null|string
     */
    public function getContent($raw = false) {
        return $this->getMainText($raw);
    }

    /**
     * @todo    refactor rendering/tpl_data fetching
     *
     * @param bool $raw
     *
     * @example usage in twig template e.g.: {{ content.getActiveArticle().getgetMainText()|raw }}
     *
     * @return null|string
     */
    public function getMainText($raw = false) {
        try {
            // check for cached values
            if(null === $this->mainText_parsed) {
                // when content not parsed but raw existing there was some error/should not be parsed
                if(null === $this->mainText_raw) {
                    // check for existing raw content
                    if(null !== ($mainText_raw = file_get_contents($this->mainText_file))) {
                        if(0 === strlen(trim($mainText_raw))) {
                            throw new \Exception('raw content is empty for id: `' . $this->id . '` with file `' . $this->mainText_file . '`');
                        }

                        $this->mainText_raw = $mainText_raw;

                        if($raw) {
                            return $this->mainText_raw;
                        } else {
                            // todo: add conditional parsing of templates
                            if(null === $this->renderer_text) {
                                throw new \Exception('renderer is not set for id: `' . $this->id . '`');
                            }
                            if(null === $this->parser) {
                                throw new \Exception('parser is not set for id: `' . $this->id . '`');
                            }

                            $template_parsed = call_user_func_array($this->renderer_text, [$this->mainText_raw]);
                            $this->mainText_parsed = call_user_func_array($this->parser, [$template_parsed]);
                            return $this->mainText_parsed;
                        }
                    } else {
                        throw new \Exception('raw content not found for id: `' . $this->id . '` with file `' . $this->mainText_file . '`');
                    }
                } else {
                    return $this->mainText_raw;
                }
            } else {
                return $this->mainText_parsed;
            }

        } catch(\Throwable $e) {
            echo 'Canal\Feature\Content\Article: ' . $e->getMessage() . "\r\n";
            return '';
        }
    }

    public function update($data) {
        $res = [];
        if(isset($data['mainText'])) {
            if(is_string($data['mainText'])) {
                $res = array_merge($res, $this->updateMainText($data['mainText']));
            } else {
                $res[] = [
                    'type' => 'update',
                    'code' => 'mainText-no-string',
                ];
            }
        }

        if(isset($data['meta'])) {
            if(is_array($data['meta'])) {
                $res = array_merge($res, $this->updateMeta($data['meta']));
            } else {
                $res[] = [
                    'type' => 'update',
                    'code' => 'meta-no-array',
                ];
            }
        }

        if(isset($data['docTree'])) {
            if(is_array($data['docTree'])) {
                $res = array_merge($res, $this->updateDocTree($data['docTree']));
            } else {
                $res[] = [
                    'type' => 'update',
                    'code' => 'docTree-no-array',
                ];
            }
        }

        return $res;
    }

    public function updateMainText($mainText) {
        $res = [];
        try {
            if(false !== file_put_contents($this->mainText_file, $mainText)) {
                return $res;
            } else {
                $res[] = [
                    'type' => 'update',
                    'code' => 'content-mainText-not-saved',
                    'info' => [
                        'content_file' => $this->mainText_file,
                    ],
                ];
                throw new \Exception('could not save maintext for id: `' . $this->id . '` with file `' . $this->mainText_file . '`');
            }
        } catch(\Throwable $e) {
            error_log('Canal\Feature\Content\Article.updateContent: ' . $e->getMessage() . "\r\n");
            return $res;
        }
    }

    public function updateMeta($meta) {
        $res = [];
        try {
            if(false !== $this->meta->updateFile($meta)) {
                return $res;
            } else {
                $res[] = [
                    'type' => 'update',
                    'code' => 'content-meta-not-saved',
                    'info' => [
                        'article_id' => $this->id,
                    ],
                ];
                throw new \Exception('could not save meta for id: `' . $this->id . '`');
            }
        } catch(\Throwable $e) {
            error_log('Canal\Feature\Content\Article.updateMeta: ' . $e->getMessage() . "\r\n");
            return $res;
        }
    }

    public function updateDocTree($tree) {
        $res = [];
        try {
            if(false !== $this->tree->updateFile($tree)) {
                return $res;
            } else {
                $res[] = [
                    'type' => 'update',
                    'code' => 'content-docTree-not-saved',
                    'info' => [
                        'article_id' => $this->id,
                    ],
                ];
                throw new \Exception('could not save tree for id: `' . $this->id . '`');
            }
        } catch(\Throwable $e) {
            error_log('Canal\Feature\Content\Article.updateDocTree: ' . $e->getMessage() . "\r\n");
            return $res;
        }
    }
}