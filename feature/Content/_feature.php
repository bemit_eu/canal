<?php

use Flood\Captn\EventDispatcher;
use Flood\Canal\Service\Md\Parser as MdParser;
use Flood\Canal\Feature\Content\Content;
use Flood\Canal\Feature\Content\SchemaStore;
use Flood\Canal\Feature\Content\PageTypeStore;
use Flood\Canal\Feature\Content\DocTree\BlockStore;

/**
 * @param $frontend \Flood\Canal\Frontend
 *
 * @return \Flood\Canal\Feature\FileI
 */
return static function($frontend) {
    return new class($frontend) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {
        public function __construct($frontend) {
            parent::__construct($frontend);

            $this->name('content');

            $this->task('install', [$this, 'install']);
            $this->task('uninstall', [$this, 'uninstall']);
            EventDispatcher::on('core.init', [$this, 'init']);
            EventDispatcher::on('core.console.init', [$this, 'initConsole']);
        }

        public function install() {

        }

        public function uninstall() {

        }

        public function init() {
            $route = require __DIR__ . '/_routeApi.php';
            $route($this->frontend->route);

            \Flood\Canal\Controller\Content::$md = new MdParser();

            $this->frontend->content = new Content();

            Content::$path = $this->frontend->path_data . 'content/';
            SchemaStore::$storage_path = Content::$path . '_schema/';
            PageTypeStore::$storage_path = Content::$path . '_page-type/';
            BlockStore::$path = Content::$path . BlockStore::$path_slug . '/';

            $this->frontend->addViewDir(Content::$path, 'content')
                           ->addViewDir(BlockStore::$path, 'content_block');
        }

        public function initConsole(\Flood\Canal\Console $console) {
            $console::addScope('content', static function() {
                return new Flood\Canal\Feature\Cache\Console\Cache();
            });

            return $console;
        }
    };
};