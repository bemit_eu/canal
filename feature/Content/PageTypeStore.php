<?php

namespace Flood\Canal\Feature\Content;

/**
 * PageTypeStore Storage
 */
class PageTypeStore {
    static public $storage_path = '';

    protected $id;

    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @throws \Exception
     *
     * @return mixed
     */
    public function get() {
        $pagetype_file = static::$storage_path . $this->id . '.json';

        if(is_file($pagetype_file)) {
            if(null !== ($pagetype = json_decode(file_get_contents($pagetype_file), true))) {
                return $pagetype;
            } else {
                throw new \Exception('Canal\Feature\Content\PateTypeStore: json not readable: ' . $this->id);
            }
        } else {
            throw new \Exception('Canal\Feature\Content\PateTypeStore: file not found: ' . $this->id);
        }
    }
}