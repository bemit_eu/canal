<?php

namespace Flood\Canal\Feature\Content;


class Index {
    /**
     * @var array [ 'id' => section_obj, 'id2' => section_obj,]
     */
    protected $section = [];
    /**
     * @var array [ 'section_id' => ['article_id', 'article_id2'], ]
     */
    protected $section_article = [];

    /**
     * @var array [ 'id' => block_obj, 'id2' => block_obj,]
     */
    protected $block = [];

    /**
     * @var array [ 'id' => pageType_obj, 'id2' => pageType_obj,]
     */
    protected $page_type = [];

    /**
     * @var array only list with all registered article ID's [ 'art_id', 'art_id1', ]
     */
    protected $article_list = [];

    /**
     * @var array [ 'path' => article_obj, ]
     */
    protected $path_2_article = [];
    /**
     * @var array [ 'marker' => article_obj, ]
     */
    protected $marker_2_article = [];
    /**
     * @var array [ 'route' => article_obj, ]
     */
    protected $route_2_article = [];
    /**
     * @var array [ ['id' => '<id>', 'art' => article_obj,] ]
     */
    protected $autowire_article = [];

    /**
     * @var array ['article_id' => 'section'] is filled in Section->addArticle
     *            This is used to fast determine:
     *            - which article exists in which section
     *            - if an article already exists with that id, article IDs need to be globally unique
     *            - which content should be loaded, the ID of the article is used as file name and selector,
     *            it could be the path part of url, the ID of the route and also match results
     *
     *            as going through all sections and then articles to see if exists would be need to long.
     */

    /**
     * @param $id
     * @param $obj
     *
     * @return \Flood\Canal\Feature\Content\Section
     */
    public function addSection($id, $obj) {
        $this->section[$id] = $obj;
        return $obj;
    }

    /**
     * @param $id
     * @param $obj
     *
     * @return \Flood\Canal\Feature\Content\DocTree\BlockStore
     */
    public function addBlock($id, $obj) {
        $this->block[$id] = $obj;
        return $obj;
    }

    /**
     * @param $id
     *
     * @return \Flood\Canal\Feature\Content\DocTree\BlockStore
     */
    public function getBlock($id) {
        return $this->block[$id];
    }

    /**
     * @return \Flood\Canal\Feature\Content\DocTree\BlockStore[]
     */
    public function getBlocks() {
        return $this->block;
    }

    /**
     * @param $id
     * @param $obj
     *
     * @return \Flood\Canal\Feature\Content\PageTypeStore
     */
    public function addPageType($id, $obj) {
        $this->page_type[$id] = $obj;
        return $obj;
    }

    /**
     * @param $id
     *
     * @return \Flood\Canal\Feature\Content\PageTypeStore
     */
    public function getPageType($id) {
        return $this->page_type[$id];
    }

    /**
     * @param $id
     *
     * @return \Flood\Canal\Feature\Content\Section|array
     */
    public function getSection($id = null) {
        if(null === $id) {
            return $this->section;
        } else {
            return $this->section[$id];
        }
    }

    public function addArticle($section_id, $id, $tag, $obj) {
        $this->section_article[$section_id][$id] = $obj;
        $this->article_list[] = $id;

        if(empty($tag)) {
            if(isset($this->autowire_article[$id])) {
                error_log('Canal\Feature\Content\Index: added duplicate article to `autowire` with article id `' . $id . '` in section `' . $section_id . '`');
            }
            $this->autowire_article[$id] =& $this->section_article[$section_id][$id];
        } else {
            if(isset($tag['path'])) {
                $this->path_2_article[$tag['path']] =& $this->section_article[$section_id][$id];
            }

            if(isset($tag['marker'])) {
                if(is_string($tag['marker'])) {
                    $this->marker_2_article[$tag['marker']] =& $this->section_article[$section_id][$id];
                } else if(is_array($tag['marker'])) {
                    foreach($tag['marker'] as $marker) {
                        $this->marker_2_article[$marker] =& $this->section_article[$section_id][$id];
                    }
                }
            }

            if(isset($tag['route'])) {
                $this->route_2_article[$tag['route']] =& $this->section_article[$section_id][$id];
            }
        }

        return $this->section_article[$section_id][$id];
    }

    /**
     * @param $id
     * @param $section_id
     *
     * @return \Flood\Canal\Feature\Content\Article|array
     */
    public function getArticle($section_id, $id = null) {
        if(null === $id) {
            return $this->section_article[$section_id];
        } else {
            return $this->section_article[$section_id][$id];
        }
    }

    /**
     * @param $path
     *
     * @return \Flood\Canal\Feature\Content\Article|array
     */
    public function getArticleByPath($path) {
        return $this->path_2_article[$path];
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function existArticleByPath($path) {
        if(isset($this->path_2_article[$path])) {
            return true;
        }

        return false;
    }

    /**
     * @param $route
     *
     * @return \Flood\Canal\Feature\Content\Article|array
     */
    public function getArticleByRoute($route) {
        return $this->route_2_article[$route];
    }

    /**
     * @param $route
     *
     * @return bool
     */
    public function existArticleByRoute($route) {
        if(isset($this->route_2_article[$route])) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     *
     * @return \Flood\Canal\Feature\Content\Article|array
     */
    public function getArticleInAutowire($id) {
        return $this->autowire_article[$id];
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function existArticleInAutowire($id) {
        if(isset($this->autowire_article[$id])) {
            return true;
        }

        return false;
    }


    /**
     * @param $id
     *
     * @return bool
     */
    public function existSection($id) {
        return array_key_exists($id, $this->section);
    }

    /**
     * Checks if an article exists, when `section` is not set it will be checked if any article in any section has the `id`
     *
     * @param $id
     * @param $section
     *
     * @return bool
     */
    public function existArticle($id, $section = null) {
        if(null === $section) {
            return array_key_exists($id, $this->article_list);
        } else if(isset($this->section_article[$section])) {
            return array_key_exists($id, $this->section_article[$section]);
        } else {
            return false;
        }
    }
}