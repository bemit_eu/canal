<?php

namespace Flood\Canal\Feature\Content;


class DocTree {

    /**
     * @var array
     */
    protected $data = false;

    /**
     * @var string
     */
    protected $file = '';

    /**
     * @var callable that received the raw content and processes the template language
     */
    protected $renderer_text = null;
    protected $renderer_file = null;
    /**
     * @var callable that receive the rendered template and will parse the content
     */
    protected $parser = null;

    /**
     * @var \Flood\Canal\Feature\Content\Article
     */
    protected $article;

    /**
     * @param $file
     * @param $article
     */
    public function __construct($file, &$article) {
        $this->file = $file . '_doc-tree.json';
        $this->article = &$article;
    }

    /**
     * @param callable $renderer_text template language parsing, e.g. Twig Text
     * @param callable $renderer_file template file parsing, e.g. Twig File
     * @param callable $parser        content language parsing, e.g. MarkDown
     */
    public function provideRenderEnv(&$renderer_text, &$renderer_file, &$parser) {
        $this->renderer_text = &$renderer_text;
        $this->renderer_file = &$renderer_file;
        $this->parser = &$parser;
    }

    /**
     * @throws \Exception
     */
    public function loadFile() {
        if(is_file($this->file)) {
            if(null !== ($tree = json_decode(file_get_contents($this->file), true))) {
                $this->data = $tree;
            } else {
                throw new \Exception('Canal\Feature\Content\ArticleTree: json not readable in `' . $this->file . '`');
            }
        } else {
            throw new \Exception('Canal\Feature\Content\ArticleTree: `' . $this->file . '` file not found.');
        }
    }

    public function createFile($data) {
        if(!is_file($this->file)) {
            $folder = strrev(substr(strrev($this->file), strpos(strrev($this->file), '/')));
            if(!is_dir($folder)) {
                mkdir($folder);
            }
            if(false === file_put_contents($this->file, json_encode($data))) {
                error_log('Canal\Feature\Content\ArticleTree: file could not be created: ' . $this->file);
            } else {
                $this->loadFile();
                error_log('Canal\Feature\Content\ArticleTree: + created file: ' . $this->file);
                return true;
            }
        }
        return false;
    }

    /**
     * @param $tree
     *
     * @return bool
     *
     * @throws
     */
    public function updateFile($tree) {
        if(is_file($this->file)) {
            // todo: optimize pretty-print saving (with two blanks)
            $json_indented_by_4 = json_encode($tree, JSON_PRETTY_PRINT);
            if(false !== file_put_contents($this->file, preg_replace('/^(  +?)\\1(?=[^ ])/m', '$1', $json_indented_by_4))) {
                return true;
            }
        } else {
            throw new \Exception('Canal\Feature\Content\ArticleDocTree: `' . $this->file . '` file not found.');
        }
        return false;
    }

    /**
     * Returns the definition for one article
     *
     * @param null $root_block
     *
     * @return array
     */
    public function getDefinition($root_block = null) {
        try {
            if(false === $this->data) {
                $this->loadFile();
            }
        } catch(\Exception $e) {
            return [];
        }

        if(null === $root_block) {
            return $this->data;
        } else if(isset($this->data[$root_block])) {
            return $this->data[$root_block];
        }
        return [];
    }

    /**
     * Get list of fragments in this tree or one rendered fragment
     *
     * @param $name
     * @param $text
     *
     * @throws \Exception
     *
     * @return null|array|string
     */
    public function getRootBlock($name, $text = true) {
        try {
            if(false === $this->data) {
                $this->loadFile();
            }
        } catch(\Exception $e) {
            return null;
        }

        if(isset($this->data[$name])) {
            return
                (new DocTree\BlockRenderer($name, $this->data[$name], $this->article))
                    ->render(
                        $text,
                        $this->renderer_file,
                        $this->renderer_text,
                        $this->parser
                    );
        }

        return null;
    }
}