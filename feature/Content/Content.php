<?php

namespace Flood\Canal\Feature\Content;

use Flood\Canal\Url\Url;

class Content {
    /**
     * @var string path to folder containing all section folders
     */
    static public $path = '';

    /**
     * @var \Flood\Canal\Feature\Content\Index
     */
    public $index;

    /**
     * @var \Flood\Canal\Feature\Content\SchemaStore
     */
    public $schema;

    /**
     * @var \Flood\Canal\Feature\Content\Article
     */
    protected $active_article;

    public function __construct() {
        $this->index = new Index();
        $this->schema = new SchemaStore();
    }

    /**
     * This implements a fluent interface.
     *
     * @param $id
     *
     * @return \Flood\Canal\Feature\Content\Section
     */
    public function addSection($id) {
        return $this->index->addSection($id, new Section($id, $this->index, static::$path));
    }

    /**
     * This implements a fluent interface.
     *
     * @param $id
     *
     * @return self
     */
    public function addBlock($id) {
        $this->index->addBlock($id, new DocTree\BlockStore($id));
        return $this;
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function addPageType($id) {
        $this->index->addPageType($id, new PageTypeStore($id));
        return $this;
    }

    /**
     * Returns an Article, if exist
     *
     * @param $section
     * @param $id
     *
     * @return bool|\Flood\Canal\Feature\Content\Article
     */
    public function getArticle($section, $id) {
        $return_val = false;
        if($this->index->existSection($section)) {
            $return_val = $this->index->getArticle($section, $id);
        }

        return $return_val;
    }

    /**
     * @param string $by
     * @param string $direction
     *
     * @todo implement the sorting `by` a value, currently date.update is hardcoded, needs a string lexer
     *
     * @return array
     */
    public function getSorted($by = '_date.update', $direction = 'ASC') {
        $sorted = [];
        foreach($this->getSection() as $section_id => $section) {
            /**
             * @var \Flood\Canal\Feature\Content\Section $section
             */
            foreach($section->getArticle() as $article_id => $article) {
                /**
                 * @var \Flood\Canal\Feature\Content\Article $article
                 */
                if(array_key_exists($article->meta('_date')['update'], $sorted)) {
                    $sort_key = $article->meta('_date')['update'] . "#" . rand(1000, 9999);
                } else {
                    $sort_key = $article->meta('_date')['update'];
                }
                $sorted[$sort_key] = [
                    'article'    => $article,
                    'article_id' => $article_id,
                    'section_id' => $section_id,
                ];
            }
        }
        switch($by) {
            case 'date':
                break;
        }
        uksort($sorted, 'strnatcmp');

        if('ASC' === $direction) {
            $sorted = array_reverse($sorted, true);
        }

        return $sorted;
    }

    /**
     * Returns an Section, if exist
     *
     * @param $id
     *
     * @return bool|\Flood\Canal\Feature\Content\Section
     */
    public function getSection($id = null) {
        $return_val = false;
        if($this->index->existSection($id)) {
            $return_val = $this->index->getSection($id);
        }

        return $return_val;
    }

    /**
     * Will get the content with fetching the ID from the request and match
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param array                                     $match
     *
     * @return \Flood\Canal\Feature\Content\Article
     */
    public function match($request, $match) {
        $matched = false;
        try {
            $url = new Url();
            if($this->index->existArticleByPath($url->stripSlash($request->getPathInfo()))) {
                $this->active_article = $this->index->getArticleByPath($url->stripSlash($request->getPathInfo()));
                $matched = true;
            }

            if(!$matched && $this->index->existArticleByRoute($match['_route'])) {
                $this->active_article = $this->index->getArticleByRoute($match['_route']);
                $matched = true;
            }

            if(!$matched && $this->index->existArticleInAutowire($url->stripSlash($request->getPathInfo()))) {
                $this->active_article = $this->index->getArticleInAutowire($url->stripSlash($request->getPathInfo()));
                $matched = true;
            }

            if(!$matched && $this->index->existArticleInAutowire($match['_route'])) {
                $this->active_article = $this->index->getArticleInAutowire($match['_route']);
                $matched = true;
            }

            /*if ($this->existArticle($match['_route']) && !$matched) {
                //$this->active_article_id = $match['_route'];
                //$matched = true;
            }*/

            if(!$matched) {
                $msg = 'Canal: no content match for active request:' . "\r\n";;
                $msg .= 'tried path `' . $url->stripSlash($request->getPathInfo()) . '` as `id`' . "\r\n";
                $msg .= 'tried route id `' . $match['_route'] . '` as `id`.' . "\r\n";
                throw new \Exception($msg);
            }
        } catch(\Exception $e) {
            echo $e->getMessage() . "\r\n";
            exit(1);
        }

        return $this->active_article;
    }

    /**
     * @return \Flood\Canal\Feature\Content\Article
     */
    public function getActiveArticle() {
        return $this->active_article;
    }
}