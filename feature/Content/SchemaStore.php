<?php

namespace Flood\Canal\Feature\Content;

/**
 * SchemaStore Storage
 *
 * @package Flood\Canal\Feature\Content
 */
class SchemaStore {
    static public $storage_path = '';

    /**
     * @var array
     */
    public $meta = [];

    public function __construct() {
    }

    /**
     * @param $name
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function get($name) {
        $schema_file = static::$storage_path . $name . '.json';
        if(is_file($schema_file)) {
            if(null !== ($schema = json_decode(file_get_contents($schema_file), true))) {
                return $schema;
            } else {
                throw new \Exception('Canal\Feature\Content\Schema: schema json not readable: ' . $name);
            }
        } else {
            throw new \Exception('Canal\Feature\Content\Schema: schema file not found: ' . $name);
        }
    }
}