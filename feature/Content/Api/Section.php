<?php

namespace Flood\Canal\Feature\Content\Api;

use Flood\Canal\Feature\Api\ApiController;

class Section extends ApiController {
    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }

    /**
     * Get a list of all existing lists
     */
    public function getList() {
        $section_list = $this->frontend->content->index->getSection();

        $this->resp_data = [];
        foreach($section_list as $sec) {
            /**
             * @var \Flood\Canal\Feature\Content\Section $sec
             */
            $this->resp_data[] = [
                'id'   => $sec->id,
                'name' => $sec->id,
            ];
        }

        $this->respondJson();
    }

    /**
     * Get a list of all article in one section
     *
     * @param $section
     */
    public function getListArticle($section) {
        $article_list = $this->frontend->content->index->getArticle($section);

        $this->resp_data = [];
        foreach($article_list as $art) {
            /**
             * @var \Flood\Canal\Feature\Content\Article $art
             */
            $this->resp_data[] = [
                'id'   => $art->id,
                'name' => $art->id,
            ];
        }

        $this->respondJson();
    }
}