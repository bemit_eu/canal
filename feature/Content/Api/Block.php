<?php

namespace Flood\Canal\Feature\Content\Api;

use Flood\Canal\Feature\Api\ApiController;

class Block extends ApiController {
    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }

    /**
     * @param $id
     *
     * @throws \Exception
     */
    public function get($id) {
        $b = $this->frontend->content->index->getBlock($id);
        $this->resp_data = [];

        if($b->hasType()) {
            $this->resp_data['type'] = $b->getType();
        }
        if($b->hasSchema()) {
            $this->resp_data['schema'] = $b->getSchema();
        }
        if($b->hasTemplate()) {
            $this->resp_data['tpl'] = $b->getTemplate();
        }

        $this->respondJson();
    }

    /**
     * @throws \Exception
     */
    public function list() {
        $blocks = $this->frontend->content->index->getBlocks();

        $this->resp_data = [];
        foreach($blocks as $block) {
            $this->resp_data[] = [
                'id'   => $block->id,
                'type' => $block->getType(),
                'data' => [],
            ];
        }

        $this->respondJson();
    }
}