<?php

namespace Flood\Canal\Feature\Content\Api;

use Flood\Canal\Feature\Api\ApiController;

class Article extends ApiController {
    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }

    public function get($section, $article) {
        $article = $this->frontend->content->index->getArticle($section, $article);

        $meta = $article->meta();

        $this->resp_data = [
            'id'   => $article->id,
            'name' => $article->id,
            'data' => [
                'date_create' => isset($meta['_date']['create']) ? $meta['_date']['create'] : false,
                'date_update' => isset($meta['_date']['update']) ? $meta['_date']['update'] : false,
                'page_type'   => $article->meta('_page-type') ? $article->meta('_page-type') : false,
                'meta'        => $article->meta(),
                'mainText'    => $article->getMainText(true),
                'docTree'     => $article->getDocTreeDefinition(),
            ],
        ];

        $this->respondJson();
    }

    public function update($section, $article) {
        $article = $this->frontend->content->index->getArticle($section, $article);

        $error = [];

        if('PUT' === filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {
            $value = file_get_contents('php://input');
            $value = json_decode($value, true);
            if(is_array($value)) {
                $error = array_merge($error, $article->update($value));
            } else {
                $error[] = [
                    'type' => 'update',
                    'code' => 'wrong-input',
                ];
            }
        }

        $this->resp_data = [
            'id'    => $article->id,
            'error' => (empty($error) ? false : $error),
        ];

        $this->respondJson();
    }
}