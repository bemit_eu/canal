<?php

namespace Flood\Canal\Feature\Content\Api;

use Flood\Canal\Feature\Api\ApiController;

class PageType extends ApiController {
    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }

    /**
     * @param $id
     *
     * @throws \Exception
     */
    public function get($id) {
        $pagetype = $this->frontend->content->index->getPageType($id);
        $data = $pagetype->get();
        if($data) {
            $this->resp_data = $data;
        } else {
            $this->resp_data = false;
        }

        $this->respondJson();
    }

}