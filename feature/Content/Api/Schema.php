<?php

namespace Flood\Canal\Feature\Content\Api;

use Flood\Canal\Feature\Api\ApiController;

use Flood\Canal\Feature\Content\SchemaStore as ContentSchema;

class Schema extends ApiController {
    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend);
    }

    /**
     * @param $schema
     *
     * @throws \Exception
     */
    public function get($schema) {
        $schema = urldecode($schema);
        $s = new ContentSchema();
        if(is_string($schema)) {
            $this->resp_data = $s->get($schema);
        }

        $this->respondJson();
    }
}