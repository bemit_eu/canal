<?php

namespace Flood\Canal\Feature\Content\DocTree;

class BlockRenderer {

    /**
     * @var array
     */
    protected $id;

    /**
     * @var array
     */
    protected $data;
    /**
     * @var \Flood\Canal\Feature\Content\Article which receives template as `text` and data as `array` should return finished HTML
     */
    protected $article;

    /**
     * @param $id
     * @param $data
     * @param $article
     */
    public function __construct($id, $data, &$article) {
        $this->id = $id;
        $this->data = $data;
        $this->hasChildren = (isset($data['children']) && 0 < count($data['children']));
        $this->childrenQty = ($this->hasChildren ? count($data['children']) : 0);

        $this->article = &$article;
    }

    /**
     * @param bool          $text
     * @param null|callable $renderer_file
     * @param null|callable $renderer_text
     * @param null|callable $parser
     *
     * @return array|string
     */
    public function render($text = true, $renderer_file = null, $renderer_text = null, $parser = null) {
        $html = [];
        if($this->hasChildren) {
            $children = $this->data['children'];

            foreach($children as $i => $block) {
                if(isset($block['type'])) {
                    switch($block['type']) {
                        case 'meta':
                            $html[$i] = $this->renderMeta($block, $renderer_file, $renderer_text, $parser);
                            break;
                        case 'content':
                            $html[$i] = $this->renderBlock($block, $renderer_file, $renderer_text, $parser);
                            break;
                        case 'mainText':
                            $html[$i] = $this->article->getMainText();
                            break;
                    }
                } else {
                    error_log('ArticleTreeFragment: render, block `' . $i . '` in fragment `' . $this->id . '` has no type.');
                }
            }
        }

        if($text) {
            return implode('', $html);
        } else {
            return $html;
        }
    }

    /**
     * @param array         $block
     * @param null|callable $renderer_file
     * @param null|callable $renderer_text
     * @param null|callable $parser
     *
     * @return string
     */
    protected function renderMeta($block, $renderer_file = null, $renderer_text = null, $parser = null) {
        try {
            if(!isset($block['data'])) {
                throw new \Exception('ArticleTreeFragment: renderMeta, block in fragment `' . $this->id . '` in article `' . $this->article->id . '` has no data.');
            }
        } catch(\Exception $e) {
            error_log($e->getMessage());
            return '';
        }
        $meta = $this->article->meta($block['data']);

        if(isset($block['tpl'])) {
            if(isset($block['alias'])) {
                $data = [$block['alias'] => $meta];
            } else {
                $data = $meta;
            }

            return call_user_func_array($renderer_file, [$block['tpl'], $data]);
        }

        return $meta;
    }

    /**
     * @param array         $block
     * @param null|callable $renderer_file
     * @param null|callable $renderer_text
     * @param null|callable $parser
     *
     * @return mixed|string
     */
    protected function renderBlock($block, $renderer_file = null, $renderer_text = null, $parser = null) {
        try {
            if(!isset($block['id'])) {
                throw new \Exception('ArticleTreeFragment: renderBlock, block in fragment `' . $this->id . '` in article `' . $this->article->id . '` has no id.');
            }
            if(!isset($block['data'])) {
                throw new \Exception('ArticleTreeFragment: renderBlock, block `' . $block['id'] . '` in fragment `' . $this->id . '` in article `' . $this->article->id . '` has no data.');
            }
        } catch(\Exception $e) {
            error_log($e->getMessage());
            return '';
        }

        /**
         * @var \Flood\Canal\Feature\Content\DocTree\BlockStore $block_elem
         */
        $block_elem = $this->article->index->getBlock($block['id']);
        // todo: add a schema parser that will automatically change the data fields with e.g. the parser or the text renderer
        /*if($block_elem->hasSchema()) {
            $schema = $block_elem->getSchema();
        }*/

        if($block_elem->hasTemplate()) {
            $tpl = $block_elem->getTemplate();
            if(isset($block['data'])) {
                return call_user_func_array($renderer_file, [$tpl, ['block_data' => $block['data']]]);
            }

            return call_user_func_array($renderer_file, [$tpl]);
        }

        return '';
    }
}