<?php

namespace Flood\Canal\Feature\Content\DocTree;


class BlockStore {
    /**
     * @var string root storage path
     */
    static public $path = '';

    static public $path_slug = '_block';

    public $id;

    /**
     * @var bool|array
     */
    protected $schema = false;

    protected $info = false;

    /**
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;

        $file = static::$path . $this->id . '/block.json';

        try {
            if(is_file($file)) {
                if(null !== ($block_info = json_decode(file_get_contents($file), true))) {
                    $this->info = $block_info;
                } else {
                    throw new \Exception('Canal\Feature\Content\ArticleBlock: json not readable in `' . $file . '`');
                }
            } else {
                throw new \Exception('Canal\Feature\Content\ArticleBlock: `' . $file . '` file not found.');
            }
        } catch(\Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * @return bool
     */
    public function hasSchema() {
        if(isset($this->info['schema']) && false !== $this->info['schema']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function getSchema() {
        if(false === $this->schema) {
            if(!$this->hasSchema()) {
                return false;
            }
            $file = static::$path . $this->id . '/schema.json';
            try {
                if(is_file($file)) {
                    if(null !== ($schema = json_decode(file_get_contents($file), true))) {
                        $this->schema = $schema;
                    } else {
                        throw new \Exception('Canal\Feature\Content\ArticleBlock: json not readable in `' . $file . '`');
                    }
                } else {
                    throw new \Exception('Canal\Feature\Content\ArticleBlock: `' . $file . '` file not found.');
                }
            } catch(\Exception $e) {
                error_log($e->getMessage());
                return false;
            }
        }

        return $this->schema;
    }

    /**
     * @return bool
     */
    public function hasTemplate() {
        if(isset($this->info['tpl']) && false !== $this->info['tpl']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function getTemplate() {
        if(isset($this->info['tpl'])) {
            return $this->info['tpl'];
        } else {
            return null;
        }
    }

    /**
     * @return bool
     */
    public function hasType() {
        if(isset($this->info['type']) && false !== $this->info['type']) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function getType() {
        if(isset($this->info['type'])) {
            return $this->info['type'];
        } else {
            return null;
        }
    }
}