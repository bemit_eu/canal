<?php

namespace Flood\Canal\Feature\Content;


class Section {

    /**
     * @var string
     */
    public $id;

    /**
     * @var \Flood\Canal\Feature\Content\Index
     */
    protected $index;

    /**
     * @var string path to folder containing all article content files, relative to Content->path
     */
    protected $path = '';
    /**
     * @var string root storage path
     */
    protected $storage_path = '';

    /**
     * Content\Section constructor.
     *
     * @param                              $id
     * @param \Flood\Canal\Feature\Content\Index   $index
     * @param                              $storage_path
     */
    public function __construct($id, &$index, $storage_path) {
        $this->id = $id;
        $this->index = &$index;
        $this->storage_path = $storage_path;
    }

    /**
     * @param array $article_list ['id' => '', 'folder' => '', 'file' => '']
     */
    public function addArticleList($article_list) {
        foreach($article_list as $article) {
            $this->addArticle(
                $article['id'],
                (isset($article['tag']) ? $article['tag'] : []),
                (isset($article['folder']) ? $article['folder'] : ''),
                (isset($article['file']) ? $article['file'] : '')
            );
        }
    }

    /**
     * $folder and $file are currently not documented on painttheweb.de
     *
     * This implements a fluent interface.
     *
     * @param        $article_id
     * @param array  $tag
     * @param string $folder
     * @param string $file
     *
     * @return self
     */
    public function addArticle($article_id, $tag = [], $folder = '', $file = '') {
        $path = $this->storage_path;

        if(!empty($this->path)) {
            $path .= $this->path;
        }

        if(!empty($folder)) {
            $path .= $folder;
        }

        try {
            if(!$this->index->existArticle($article_id, $this->id)) {
                $this->index->addArticle($this->id, $article_id, $tag, new Article($article_id, $path, $file, $this->index));
            } else {
                throw new \Exception('Canal: duplicate article id, could not set one of the articles with id: ' . $article_id);
            }
        } catch(\Exception $e) {
            echo $e->getMessage() . "\r\n";
        }

        return $this;
    }

    /**
     * Especially for a one liner init of a section with articles
     *
     * This implements a fluent interface.
     *
     * @param $path
     *
     * @return $this
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * @param $id
     *
     * @return \Flood\Canal\Feature\Content\Article
     */
    public function getArticle($id = null) {
        return $this->index->getArticle($this->id, $id);
    }
}