<?php

namespace Flood\Canal\Feature\Content;


class ArticleMeta {

    /**
     * @var \Flood\Canal\Feature\Content\Article
     */
    protected $article;

    /**
     * @var array
     */
    protected $data = false;

    /**
     * @var string
     */
    protected $file = '';

    /**
     * @var array
     */
    protected static $data_access_cache = [];

    /**
     * @param $file
     * @param $article
     *
     * @throws \Exception
     */
    public function __construct($file, &$article) {
        $this->file = $file . '.json';
        $this->article = &$article;
    }

    /**
     * @throws \Exception
     */
    public function loadFile() {
        if(is_file($this->file)) {
            if(null !== ($meta = json_decode(file_get_contents($this->file), true))) {
                $this->data = $meta;
            } else {
                throw new \Exception('Canal\Feature\Content\ArticleMeta: meta json not readable in `' . $this->file . '`');
            }
        } else {
            throw new \Exception('Canal\Feature\Content\ArticleMeta: `' . $this->file . '` file not found.');
        }
    }

    /**
     * @param $meta
     *
     * @return bool
     *
     * @throws
     */
    public function updateFile($meta) {
        if(is_file($this->file)) {
            // todo: define better "defaults" which will be extracted from the existing meta
            $m = $this->get();
            if(isset($m['_schema'])) {
                $meta['_schema'] = $m['_schema'];
            }
            if(isset($m['_page-type'])) {
                $meta['_page-type'] = $m['_page-type'];
            }
            if(isset($m['_doc-tree'])) {
                $meta['_doc-tree'] = $m['_doc-tree'];
            }
            // todo: optimize pretty-print saving (with two blanks)
            $json_indented_by_4 = json_encode($meta, JSON_PRETTY_PRINT);
            if(false !== file_put_contents($this->file, preg_replace('/^(  +?)\\1(?=[^ ])/m', '$1', $json_indented_by_4))) {
                return true;
            }
        } else {
            throw new \Exception('Canal\Feature\Content\ArticleMeta: `' . $this->file . '` file not found.');
        }
        return false;
    }

    public function createFile($data) {
        if(!is_file($this->file)) {
            $folder = strrev(substr(strrev($this->file), strpos(strrev($this->file), '/')));
            if(!is_dir($folder)) {
                mkdir($folder);
            }
            if(false === file_put_contents($this->file, json_encode($data))) {
                error_log('Canal\Feature\Content\ArticleMeta: meta file could not be created: ' . $this->file);
            } else {
                error_log('Canal\Feature\Content\ArticleMeta: + created meta file: ' . $this->file);
                return true;
            }
        }
        return false;
    }

    /**
     * @param null|string|array $key
     *
     * @todo add dot or array selector (key.key1.key2 and ['key','key1','key2',]
     *
     * @return array|mixed|null
     */
    public function get($key = null) {
        try {
            if(false === $this->data) {
                $this->loadFile();
            }
        } catch(\Exception $e) {
            return null;
        }

        if(null === $key) {
            return $this->data;
        } else {
            return $this->getData($key);
        }/* else {
            return null;
        }*/
    }

    /**
     * Dot Selector
     *
     * @param string|array $key
     *
     * @todo uncached dot selector may be time consuming for all meta calls
     *
     * @return mixed|null
     */
    protected function getData($key = []) {
        if(is_string($key)) {
            $key = explode('.', $key);
        }
        // concat all submitted keys to use as one index in the cache array
        $key_cache = implode('.', $key);

        // check if the config was already cached and return the cached value
        if(isset(static::$data_access_cache[$this->article->id][$key_cache])) {
            return static::$data_access_cache[$this->article->id][$key_cache];
        }
        // var_dump(static::$data);
        if(isset($this->data[$key[0]])) {
            $setting_scope = [];
            // zero index is the base setting
            $setting_scope[0] = &$this->data[$key[0]];
            $iteration_key = $key;
            unset($iteration_key[0]);// unset the first key, as this selection was done now
            $iteration_key = array_values($iteration_key);

            if(is_array($setting_scope)) {
                // go through each key
                for($i = 1; $i <= count($iteration_key); $i++) {
                    // and set the next index with the value of the key
                    // {new_scope}[index] = {scope, one index previous}[key for this iteration]
                    $setting_scope[$i] = &$setting_scope[$i - 1][$iteration_key[$i - 1]];
                }
                // result is the latest setting scope
                $result = &$setting_scope[count($setting_scope) - 1];
                // setting with result the access cache
                if(!isset(static::$data_access_cache[$this->article->id])) {
                    static::$data_access_cache[$this->article->id] = [];
                }
                static::$data_access_cache[$this->article->id][$key_cache] = &$result;
            } else {
                $result = null;
            }
        } else {
            $result = null;
        }

        return $result;
    }
}