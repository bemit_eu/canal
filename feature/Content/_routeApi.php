<?php

use \Flood\Canal\Controller\ApiRoute;

/**
 * @param \Flood\Canal\Route\Routing $route
 */
return function($route) {

    ///
    // Section
    ///

    (new ApiRoute('content--section-list', '/content/section'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Section($frontend);
            $controller->getList();
        });

    (new ApiRoute('content--section-article-list', '/content/section/{section_id}/article'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Section($frontend);
            $controller->getListArticle($frontend->match['section_id']);
        });

    ///
    // Article
    ///

    (new ApiRoute('content--article', '/content/article/{sid}/{aid}'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Article($frontend);
            $controller->get($frontend->match['sid'], $frontend->match['aid']);
        })
        ->put(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Article($frontend);
            $controller->update($frontend->match['sid'], $frontend->match['aid']);
        });

    ///
    // Schema
    ///

    (new ApiRoute('content--schema', '/content/schema/{schema}'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Schema($frontend);
            $controller->get($frontend->match['schema']);
        });

    ///
    // PageType
    ///

    (new ApiRoute('content--page-type', '/content/page-type/{id}'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\PageType($frontend);
            $controller->get($frontend->match['id']);
        });

    ///
    // Block
    ///

    (new ApiRoute('content--block', '/content/block/{block}'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Block($frontend);
            $controller->get($frontend->match['block']);
        });

    // list available blocks, maybe constrained to a `root blocks accepts X type/flavour combination
    (new ApiRoute('content--block-selection', '/content/blocks'))
        ->setRouter($route)
        ->get(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Content\Api\Block($frontend);
            $controller->list();
        });
};