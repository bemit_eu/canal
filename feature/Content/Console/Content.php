<?php

namespace Flood\Canal\Feature\Content\Console;

use Flood\Canal\Console;

/**
 * Class Cache
 *
 * @package   Flood\Canal\Console
 * @link      https://painttheweb.de/flood-canal/console-cli-mode
 * @author    Michael Becker
 * @copyright 2018 bemit UG (haftungsbeschraenkt)
 */
class Content {

    public function autocreate($path = null) {
        if(null === $path) {
            \Flood\Canal\writeln('Content autocreate meta and tree files.');
        } else {
            \Flood\Canal\writeln('Content autocreate meta and tree files with defaults from `' . $path . '`');
        }

        $meta_data = [];
        $tree_data = [];

        if(null !== $path) {
            $path = getcwd() . '/' . $path;

            if(is_file($path)) {
                if(null !== ($data = json_decode(file_get_contents($path), true))) {
                    if(isset($data['meta'])) {
                        $meta_data = $data['meta'];
                    }
                    if(isset($data['tree'])) {
                        $tree_data = $data['tree'];
                    }
                } else {
                    \Flood\Canal\writeln('Can not decode default file: ' . $path);
                }
            }
        }

        foreach(Console::$frontend->content->index->getSection() as $section) {
            /**
             * @var \Flood\Canal\Feature\Content\Section $section
             */
            foreach($section->getArticle() as $article) {
                /**
                 * @var \Flood\Canal\Feature\Content\Article $article
                 */
                if($article->metaCreate($meta_data)) {
                    \Flood\Canal\writeln('created meta file in article `' . $article->id . '`');
                }
            }
        }

        \Flood\Canal\writeln('finished content autocreate.');
    }
}