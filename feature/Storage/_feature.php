<?php

use Flood\Captn\EventDispatcher;
use Flood\Canal\Storage\Storage;
use Flood\Canal\Feature\Storage\Json;

/**
 * @param $frontend \Flood\Canal\Frontend
 *
 * @return \Flood\Canal\Feature\FileI
 */
return static function($frontend) {
    return new class($frontend) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {

        public function __construct($frontend) {
            parent::__construct($frontend);

            $this->name('storage');

            EventDispatcher::on('core.init', [$this, 'init']);
        }

        public function install() {

        }

        public function uninstall() {

        }

        public function init() {
            Json::$db_path =& $this->frontend->path_data;
            Storage::$storage_handler[Storage::STORAGE_JSON] = Json::class;
        }
    };
};