<?php

namespace Flood\Canal\Feature\Storage;

use Symfony\Component\Filesystem\Filesystem;

/**
 * Simple JSON Storage Manager
 *
 * Stores data in JSON files and reads values from them.
 *
 * Data Structure:
 * - list: are separate data groups with items and saved with one ID, which must be a valid folder name
 * - item: is saved in a list and identified by an ID, which must be a valid file name
 * - data: is any array that will be associated with an item
 *
 * @package Flood\Canal\Storage
 */
class Json implements \Flood\Canal\Storage\Handler {
    public static $db_path = '';
    protected static $cache = [];
    protected static $index = [];
    /**
     * @var \Flood\Canal\Storage\Storage
     */
    protected $storage;

    /**
     * @param $storage \Flood\Canal\Storage\Storage
     */
    public function __construct($storage) {
        $this->storage = $storage;
    }

    public function getList($list) {
        $val = $this->read($list);
        return $val;
    }

    public function getItem($list, $item = null, $condition = null) {
        $val = $this->read($list, $item);
        return $val;
    }

    /**
     * @param $list
     * @param $item
     *
     * @return array
     */
    protected function read($list, $item = null) {
        if(null === $item) {
            return $this->readList($list);
        } else {
            return $this->readItem($list, $item);
        }
    }

    protected function readList($list) {
        $path = static::$db_path . $list;
        try {
            if(is_dir($path)) {
                $iterator = new \FilesystemIterator($path, \FilesystemIterator::SKIP_DOTS);
                $list_v = [];
                foreach($iterator as $file) {
                    if($file->isFile() && $file->getExtension() === 'json') {
                        $list_v[] = str_replace('.json', '', str_replace($path . DIRECTORY_SEPARATOR, '', $file));
                    }
                }
                return $list_v;
            } else {
                throw new \Exception('Canal\\Storage\\Json: readList could not find list `' . $list . '`');
            }
        } catch(\Exception $e) {
            return [];
        }
    }

    protected function readItem($list, $item) {
        if(isset(static::$cache[$list][$item])) {
            return static::$cache[$list][$item];
        } else {
            $path = static::$db_path . $list . '/' . $item . '.json';
            try {
                if(is_file($path)) {
                    if(false !== ($content = file_get_contents($path))) {
                        static::$cache[$list][$item] = json_decode($content, true);
                        if(!is_array(static::$cache[$list][$item])) {
                            throw new \Exception('Canal\\Storage\\Json: read decoded list `' . $list . '` with item `' . $item . '` has wrong type, is not array.');
                        }
                        return static::$cache[$list][$item];
                    } else {
                        throw new \Exception('Canal\\Storage\\Json: read could not read list `' . $list . '` with item `' . $item . '`');
                    }
                } else {
                    throw new \Exception('Canal\\Storage\\Json: read could not find list `' . $list . '` with item `' . $item . '`');
                }
            } catch(\Exception $e) {
                error_log($e->getMessage());
                return [];
            }
        }
    }

    public function hasList($list) {
        if(isset(static::$cache[$list]) || is_dir(static::$db_path . $list)) {
            return true;
        } else {
            return false;
        }
    }

    public function hasItem($list, $item) {
        if(isset(static::$cache[$list][$item]) || is_file(static::$db_path . $list . '/' . $item . '.json')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $list
     *
     * @return bool
     * @throws \Exception
     */
    public function createList($list) {
        $path = static::$db_path . $list . '/';

        if(!file_exists($path)) {
            mkdir($path);
        }

        if(!file_exists($path . '/.gitkeep')) {
            if(false !== file_put_contents($path . '/.gitkeep', '')) {
                return true;
            } else {
                throw new \Exception('Canal\\Storage\\Json: read could not write list `' . $list . '`');
            }
        } else {
            throw new \Exception('Canal\\Storage\\Json: can not create existing list `' . $list . '`');
        }
    }

    /**
     * @param string      $list
     * @param null|string $item
     * @param null|array  $data
     *
     * @return bool
     * @throws \Exception
     */
    public function createItem($list, $item = null, $data = null) {
        $path = static::$db_path . $list . '/' . $item . '.json';

        if(!is_file($path)) {
            if(!is_array($data)) {
                throw new \Exception('Canal\\Storage\\Json: createItem, wrong type of `data` for item `' . $item . '` in list `' . $list . '`, is not array.');
            }
            if(false !== file_put_contents($path, json_encode($data))) {
                static::$cache[$list][$item] = $data;
                return true;
            } else {
                throw new \Exception('Canal\\Storage\\Json: createItem, could not write item `' . $item . '` in list `' . $list . '`');
            }
        } else {
            throw new \Exception('Canal\\Storage\\Json: createItem, can not create existing item `' . $item . '` in list `' . $list . '`');
        }
    }

    /**
     * @param string     $list
     * @param string     $item
     * @param array      $data
     * @param null|array $condition
     *
     * @return bool
     * @throws \Exception
     */
    public function updateItem($list, $item, $data, $condition = null) {
        $path = static::$db_path . $list . '/' . $item . '.json';

        if(is_file($path)) {
            if(!is_array($data)) {
                throw new \Exception('Canal\\Storage\\Json: updateItem, wrong type of `data` for item `' . $item . '` in list `' . $list . '`, is not array.');
            }

            $old = $this->storage->get($list, $item, $condition);
            if(!is_array($old)) {
                throw new \Exception('Canal\\Storage\\Json: updateItem, could not get existing data for item `' . $item . '` in list `' . $list . '`');
            }

            $data = \Flood\Component\Func\Array_::merge_recursive_distinct($old, $data);

            if(false !== file_put_contents($path, json_encode($data))) {
                static::$cache[$list][$item] = $data;
                return true;
            } else {
                throw new \Exception('Canal\\Storage\\Json: createItem, could not write item `' . $item . '` in list `' . $list . '`');
            }
        } else {
            throw new \Exception('Canal\\Storage\\Json: updateItem, can not update non-existing item `' . $item . '` in list `' . $list . '`');
        }
    }

    /**
     * @param $list
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteList($list) {
        $fs = new Filesystem;

        if(!file_exists(static::$db_path . $list)) {
            return true;
        }
        if(!is_dir(static::$db_path . $list)) {
            throw new \Exception('Canal\\Storage\\Json: deleteList, can not delete list `' . $list . '`, not an folder');
        }

        $fs->remove(static::$db_path . $list);
        unset(static::$cache[$list]);
        return true;
    }

    /**
     * @param $list
     * @param $item
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteItem($list, $item) {
        $path = static::$db_path . $list . '/' . $item . '.json';

        if(!file_exists($path)) {
            return true;
        }

        if(!is_file($path)) {
            throw new \Exception('Canal\\Storage\\Json: deleteItem, could not delete item `' . $item . '` in list `' . $list . '`, not a file.');
        }

        $fs = new Filesystem;
        $fs->remove($path);
        unset(static::$cache[$list][$item]);
        return true;
    }
}