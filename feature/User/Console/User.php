<?php

namespace Flood\Canal\Feature\User\Console;

use Flood\Canal\Console;

/**
 * Class User
 *
 * @package   Flood\Canal\Console
 * @link      https://painttheweb.de/flood-canal/console-cli-mode
 * @author    Michael Becker
 * @copyright 2018 bemit UG (haftungsbeschraenkt)
 */
class User {
    public function activate() {
        if(\Flood\Canal\Feature\User\User::activate()) {
            \Flood\Canal\writeln('User activated');
        } else {
            \Flood\Canal\writeln('User could not be activated');
        }
    }

    public function deactivate() {
        if(\Flood\Canal\Feature\User\User::deactivate()) {
            \Flood\Canal\writeln('User activated');
        } else {
            \Flood\Canal\writeln('User could not be activated');
        }
    }

    public function delete($name) {
        $user = new \Flood\Canal\Feature\User\User();
        if($user->delete($name)) {
            \Flood\Canal\writeln('User delete');
        } else {
            \Flood\Canal\writeln('User could not be delete');
        }
    }

    public function create($name, $pass) {
        if(!Console::$silence) {
            \Flood\Canal\writeln('User creation of: ' . $name);
        }
        $user = new \Flood\Canal\Feature\User\User();
        if($user->create($name, $pass)) {
            \Flood\Canal\writeln('User created.');
        } else {
            \Flood\Canal\writeln('User not created.');
        }
    }

    public function updatePass($name, $pass) {
        if(!Console::$silence) {
            \Flood\Canal\writeln('User update password of: ' . $name);
        }
        $user = new \Flood\Canal\Feature\User\User();
        if($user->updatePass($name, $pass)) {
            \Flood\Canal\writeln('User password updated.');
        } else {
            \Flood\Canal\writeln('User password not updated.');
        }
    }

    public function verify($name, $pass) {
        $user = new \Flood\Canal\Feature\User\User();
        if($user->verify($name, $pass)) {
            \Flood\Canal\writeln('User successfully authenticated.');
        } else {
            \Flood\Canal\writeln('User can not be authenticated.');
        }
    }

    public function get($name) {
        $user = new \Flood\Canal\Feature\User\User();
        if('*' === $name) {
            $user_list = $user->get();
            if(empty($user_list)) {
                \Flood\Canal\writeln('No user found.');
            } else {
                \Flood\Canal\writeln('User List, got `' . count($user_list) . '` user' . (count($user_list) === 1 ? '' : 's') . ' in total:');
                foreach($user_list as $val) {
                    \Flood\Canal\writeln($val);
                }
            }
        } else {
            $user_info = $user->get($name);
            if(empty($user_info)) {
                \Flood\Canal\writeln('User `' . $name . '` not found.');
            } else {
                \Flood\Canal\writeln('User `' . $name . '` info:');
                foreach($user_info as $key => $val) {
                    \Flood\Canal\writeln($key . ":\t" . '`' . $val . '`');
                }
            }
        }
    }

    public function exist($name) {
        $user = new \Flood\Canal\Feature\User\User();
        $val = $user->exists($name);
        if($val) {
            \Flood\Canal\writeln('User `' . $name . '` found.');
        } else {
            \Flood\Canal\writeln('User `' . $name . '` not found.');
        }
    }
}