<?php

namespace Flood\Canal\Feature\User\Console;

use Flood\Canal\Console;

/**
 * Class User
 *
 * @package   Flood\Canal\Console
 * @link      https://painttheweb.de/flood-canal/console-cli-mode
 * @author    Michael Becker
 * @copyright 2018 bemit UG (haftungsbeschraenkt)
 */
class UserToken {
    public function activate() {
        if(\Flood\Canal\Feature\User\Token::activate()) {
            \Flood\Canal\writeln('UserToken activated');
        } else {
            \Flood\Canal\writeln('UserToken could not be activated');
        }
    }

    public function delete() {
        if(\Flood\Canal\Feature\User\Token::deactivate()) {
            \Flood\Canal\writeln('UserToken delete');
        } else {
            \Flood\Canal\writeln('UserToken could not be delete');
        }
    }

    public function list() {
        $tok = new \Flood\Canal\Feature\User\Token();
        $tok_list = $tok->get();
        if(empty($tok_list)) {
            \Flood\Canal\writeln('No token found.');
        } else {
            \Flood\Canal\writeln('Token List, got `' . count($tok_list) . '` token in total:');
            foreach($tok_list as $tid) {
                $t = new \Flood\Canal\Feature\User\Token();
                \Flood\Canal\writeln($tid . ' - ' . round($t->getAge($tid), 1) . 'min' . "\tfrom user: " . $t->getUser($tid));
            }
        }
    }

    public function new($user, $pass) {
        if(!Console::$silence) {
            \Flood\Canal\writeln('New Token');
        }
        $token = new \Flood\Canal\Feature\User\Token();
        $u = new \Flood\Canal\Feature\User\User();
        if($u->verify($user, $pass)) {
            if($res = $token->new($user)) {
                \Flood\Canal\writeln('Token `' . $res['token'] . '` with signature `' . $res['signature'] . '` created.');
            } else {
                \Flood\Canal\writeln('Token could not be created.');
            }
        } else {
            \Flood\Canal\writeln('Token could not be created, user not verified.');
        }
    }

    public function verify($token, $signature) {
        if(!Console::$silence) {
            \Flood\Canal\writeln('Verify Token');
        }
        $utoken = new \Flood\Canal\Feature\User\Token();
        if($res = $utoken->verify($token, $signature)) {
            \Flood\Canal\writeln('Token is valid.');
        } else {
            \Flood\Canal\writeln('Token is not valid.');
        }
    }

    public function clean() {
        if(!Console::$silence) {
            \Flood\Canal\writeln('Cleaning Token');
        }
        $utoken = new \Flood\Canal\Feature\User\Token();
        if(false !== ($qty = $utoken->clean())) {
            \Flood\Canal\writeln('Token cleaned, removed ' . $qty . ' tokens, max-age is ' . \Flood\Canal\Feature\User\Token::$max_age . 'min');
        } else {
            \Flood\Canal\writeln('Token could not be cleaned.');
        }
    }
}