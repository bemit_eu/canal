<?php

namespace Flood\Canal\Feature\User;

class RightManagerTaskValue {
    public $anonym = null;
    public $authenticated = null;

    protected $user = [];
    protected $group = [];

    public function __construct($task_info) {
        if(isset($task_info['anonym'])) {
            $this->anonym = $task_info['anonym'];
        }
        if(isset($task_info['authenticated'])) {
            $this->authenticated = $task_info['authenticated'];
        }

        if(isset($task_info['user'])) {
            $this->user = $task_info['user'];
        }
        if(isset($task_info['group'])) {
            $this->group = $task_info['group'];
        }
    }

    /**
     * @param string $name
     *
     * @return bool|null
     */
    public function isUser(string $name) {
        if(array_key_exists($name, $this->user)) {
            return $this->user[$name];
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     *
     * @return bool|null
     */
    public function isGroup(string $name) {
        if(array_key_exists($name, $this->group)) {
            return $this->group[$name];
        } else {
            return null;
        }
    }
}