<?php

use \Flood\Canal\Controller\ApiRoute;

/**
 * @param \Flood\Canal\Route\Routing $route
 */
return function($route) {
    ////
    //
    // Auth API
    //
    ////

    (new ApiRoute('api-auth--verify', '/auth/verify'))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\User\Api\Auth($frontend);
            $controller->handleVerify();
        });

    (new ApiRoute('api-auth--logout', '/auth/logout'))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\User\Api\Auth($frontend);
            $controller->handleLogout();
        });

    // test route that is only valid when authenticated
    (new ApiRoute('api-auth--test', '/auth/check-allowed'))
        ->setRouter($route)
        ->post(static function($frontend) {
            $controller = new \Flood\Canal\Feature\Api\ApiController($frontend);
            return $controller->respondJson();
        });
};