<?php

namespace Flood\Canal\Feature\User;

use Flood\Canal\Storage\Storage;

class RightManager {

    /**
     * @var \Flood\Canal\Storage\Storage
     */
    protected $storage;
    /**
     * @var \Flood\Canal\Feature\User\User
     */
    protected $user;
    /**
     * @var \Flood\Canal\Feature\User\Token
     */
    protected $token;

    /**
     * RightManager constructor.
     *
     * @param \Flood\Canal\Feature\User\User  $user
     * @param \Flood\Canal\Feature\User\Token $token
     */
    public function __construct($user, $token) {
        $this->storage = new Storage;
        $this->user = $user;
        $this->token = $token;
    }

    public static function activate() {
        return (new Storage)->create('user-right');
    }

    public static function deactivate() {
        return (new Storage)->delete('user-right');
    }

    /**
     * @param      $authenticated
     * @param      $task
     * @param null $value
     *
     * @return bool
     * @throws \Exception
     */
    public function isAllowed($authenticated, $task, $value = null) {
        $task_value_allowed = null;
        $task_allowed = null;

        $task_info = new RightManagerTask($task);
        if($task_info->exist) {

            if(null !== $value) {
                if((!is_string($value) && is_numeric($value)) && (is_string($value) && !is_numeric($value))) {
                    throw new \Exception('RightManager: `value` is set but not a string.');
                }
                if($task_info->hasValue($value)) {
                    $task_value_allowed = $this->check($authenticated, $task_info->getValue($value));
                }
            }

            if(null !== $task_value_allowed) {
                return $task_value_allowed;
            } else {
                $task_allowed = $this->check($authenticated, $task_info->get());
            }
            if(null === $task_value_allowed) {
                if(false === $task_allowed) {
                    return false;
                }

                if(true === $task_allowed) {
                    return true;
                }
            }

            return false;
        } else {
            throw new \Exception('RightManager: no task-info found for `' . $task . '`.');
        }
    }

    /**
     * @param                                                 $authenticated
     * @param \Flood\Canal\Feature\User\RightManagerTaskValue $task_info
     *
     * @throws \Exception
     *
     * @return bool|null
     */
    protected function check($authenticated, $task_info) {
        if(null !== $task_info->anonym) {
            // when anonym has rights
            /**
             * @var null|bool $anonym
             */
            $anonym = $this->checkAnonym($authenticated, $task_info);
            if(null !== $anonym) {
                return $anonym;
            }
        }
        return $this->checkAuthenticated($authenticated, $task_info);
    }

    /**
     * @param                                                 $authenticated
     * @param \Flood\Canal\Feature\User\RightManagerTaskValue $task_info
     *
     * @return bool|null
     */
    protected function checkAnonym($authenticated, $task_info) {
        if(true === $task_info->anonym || (false === $task_info->anonym && false === $authenticated)) {
            // when anonym is allowed, or:
            // when anonym is forbidden, and nobody is authenticated
            return $task_info->anonym;
        } else {
            // when anonym is not set or anonym is forbidden but someone is authenticated
            return null;
        }
    }

    /**
     * @param                                                 $authenticated
     * @param \Flood\Canal\Feature\User\RightManagerTaskValue $task_info
     *
     * @throws \Exception
     * @return bool|null
     */
    protected function checkAuthenticated($authenticated, $task_info) {
        if(false === $authenticated) {
            return null;
        }

        if(true === $task_info->authenticated) {
            return true;
        } else if(false === $task_info->authenticated || null === $task_info->authenticated) {
            $user = $task_info->isUser($authenticated);
            if(null === $user || is_bool($user)) {
                if(null !== $user) {
                    return $user;
                }
            } else {
                throw new \Exception('RightManager: task info of user is not null and not bool.');
            }

            // todo get groupname
            $groupname = [];

            $false_found = false;
            foreach($groupname as $gname) {
                $group = $task_info->isGroup($gname);
                if(null === $group || is_bool($group)) {
                    if(true === $group) {
                        return true;
                    } else if(false === $group) {
                        $false_found = true;
                    }
                } else {
                    throw new \Exception('RightManager: task info of group is not null and not bool.');
                }
            }
            if($false_found) {
                return false;
            }
            // group
        }

        if(false === $task_info->authenticated) {
            return null;
        }

        return null;
    }
}