<?php

namespace Flood\Canal\Feature\User;

use ReallySimpleJWT\Token as JWT;
use Flood\Canal\Storage\Storage;

class Token {
    /**
     * @var int how long the token should be valid, in minutes
     */
    static $max_age = 90;

    // todo: add jwt/token/auth change through headers and as handler factory
    static $jwt_use = true;

    static $jwt_secret = 'SomeSecret';

    protected $storage;

    public function __construct() {
        $this->storage = new Storage;
    }

    public static function activate() {
        return (new Storage)->create('user-token');
    }

    public static function deactivate() {
        return (new Storage)->delete('user-token');
    }

    // todo: move jwt and signature token to handler factory

    public static function newJwt($user) {
        try {

            return [
                'token' => JWT::getToken($user, static::$jwt_secret, time() + (static::$max_age * 60), 'issuerIdentifier'),
            ];
        } catch(\Exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    /**
     * Validate the header with an Bearer token, extracts `Bearer ` out of header
     *
     * @param $token
     *
     * @return bool
     */
    public static function validateJwt($token) {
        if(0 !== strpos($token, 'Bearer ')) {
            error_log('Token: JWT not valid, not containing Bearer.');
            return false;
        }

        $token = str_replace('Bearer ', '', $token);
        if(1 > strlen($token)) {
            error_log('Token: JWT not valid, is empty.');
            return false;
        }
        try {

            return JWT::validate($token, static::$jwt_secret);
        } catch(\Exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }

    /**
     * Get payload of token or false
     *
     * @param $token
     *
     * @return bool
     */
    public static function payloadJwt($token) {
        if(0 !== strpos($token, 'Bearer ')) {
            error_log('Token: JWT not valid, not containing Bearer.');
            return false;
        }

        $token = str_replace('Bearer ', '', $token);
        try {

            return JWT::getPayload($token);
        } catch(\Exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }


    /**
     * @param string $user
     *
     * @return array|bool
     */
    public function new($user) {
        $time = time();

        $token = substr(hash('sha1', strrev($time) . $time . strrev($time) . str_shuffle($time)), 0, 12);
        $secret = substr(hash('sha1', $time . strrev($time) . str_shuffle($time)) . strrev($time), 0, 24);

        if($this->storage->create('user-token', $token, ['user' => $user, 'secret' => $secret, 'time' => $time])) {
            return [
                'token'     => $token,
                'signature' =>
                    $this->makeSignatur($token, $secret),
            ];
        } else {
            return false;
        }
    }

    protected function makeSignatur($token, $secret) {
        return hash_hmac('sha256', $token, base64_decode($secret));
    }

    /**
     * @param $token
     * @param $signature
     *
     * @return bool
     */
    public function verify($token, $signature) {
        $user = $this->storage->get('user-token', $token);
        if(isset($user['secret'])) {
            return $this->verifySignatur($token, $user['secret'], $signature);
        } else {
            return false;
        }
    }

    protected function verifySignatur($token, $secret, $signature) {
        return ($this->makeSignatur($token, $secret) === $signature);
    }

    public function exists($token) {
        return $this->storage->has('user-token', $token);
    }

    public function clean() {
        $token_list = $this->get();
        $i = 0;
        foreach($token_list as $token) {
            if(!$this->isValid($token)) {
                if(!$this->delete($token)) {
                    return false;
                } else {
                    $i++;
                }
            }
        }
        return $i;
    }

    public function get($token = null) {
        if(null === $token) {
            return $this->storage->get('user-token');
        } else {
            return $this->storage->get('user-token', $token);
        }
    }

    public function getUser($token) {
        return $this->get($token)['user'];
    }

    public function getAge($token) {
        return (((time() - ($this->get($token)['time'])) / 3600) * 60);
    }

    public function isValid($token) {
        if($this->getAge($token) > static::$max_age) {
            return false;
        } else {
            return true;
        }
    }

    public function delete($token) {
        return $this->storage->delete('user-token', $token);
    }
}