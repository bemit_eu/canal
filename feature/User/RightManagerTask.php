<?php

namespace Flood\Canal\Feature\User;

use Flood\Canal\Storage\Storage;

class RightManagerTask {
    public $exist = false;

    protected $value = [];
    protected $task_info;

    /**
     * @var \Flood\Canal\Storage\Storage
     */
    protected $storage;

    public function __construct($task) {
        $this->storage = new Storage;

        $this->task_info = $this->storage->get('user-right', $task);

        if(is_array($this->task_info)) {
            $this->exist = true;
        }

        if(isset($this->task_info['value'])) {
            $this->value = $this->task_info['value'];
        }
    }

    /**
     * @return \Flood\Canal\Feature\User\RightManagerTaskValue
     */
    public function get() {
        return new RightManagerTaskValue($this->task_info);
    }

    /**
     * @param string $value
     *
     * @return bool|null
     */
    public function hasValue(string $value) {
        if(array_key_exists($value, $this->value)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $value
     *
     * @return bool|\Flood\Canal\Feature\User\RightManagerTaskValue
     */
    public function getValue(string $value) {
        if(array_key_exists($value, $this->value)) {
            return new RightManagerTaskValue($this->value[$value]);
        } else {
            // todo: add to optional logging
            //error_log('RightManager: Value not defined');
            return false;
        }
    }
}