<?php

namespace Flood\Canal\Feature\User;

class AccessControl {

    public $active = false;

    /**
     * @var bool|string false when not logged in, string with user name when logged in
     */
    protected $authenticated = false;

    /**
     * @var \Flood\Canal\Feature\User\RightManager
     */
    public $right_manager;

    public function __construct() {
    }

    public function setActiveUser() {

        if(!$this->active) {
            return true;
        }

        // first try JWT
        if(Token::$jwt_use) {
            $token = filter_input(INPUT_SERVER, 'HTTP_AUTHORIZATION', FILTER_SANITIZE_STRING);
            if(null !== $token) {
                if(Token::validateJwt($token)) {
                    $payload = json_decode(Token::payloadJwt($token), true);
                    if(isset($payload['user_id'])) {
                        $this->authenticated = $payload['user_id'];
                    }
                    return true;
                }
                return false;
            }
        }

        // then token + signature
        if(!Token::$jwt_use) {
            $token = filter_input(INPUT_SERVER, 'HTTP_X_AUTH_TOKEN', FILTER_SANITIZE_STRING);
            $signature = filter_input(INPUT_SERVER, 'HTTP_X_AUTH_SIGNATURE', FILTER_SANITIZE_STRING);
            if(null !== $token && null !== $signature && false !== $token && false !== $signature) {
                $t = new Token();
                if($t->verify($token, $signature) && $t->isValid($token)) {
                    $this->authenticated = $t->getUser($token);
                    return true;
                }

                return false;
            }
        }

        // then enable session
        if(php_sapi_name() !== 'cli') {
            session_name('FSSID');
            session_start();
            // todo: implement session auth
            session_id();
            return true;
        }

        return false;
    }

    /**
     * Checks if the current user is allowed to do the wanted task, checks against RightManager Tasks
     *
     * @param      $task
     * @param null $value
     *
     * @return bool
     */
    public function allowed($task, $value = null) {
        if(!$this->active) {
            return true;
        }

        try {
            return $this->right_manager->isAllowed($this->authenticated, $task, $value);
        } catch(\Exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }
}