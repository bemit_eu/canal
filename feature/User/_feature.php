<?php

use \Flood\Captn\EventDispatcher;
use Flood\Canal\Feature\User;

/**
 * @param $frontend \Flood\Canal\Frontend
 *
 * @return \Flood\Canal\Feature\FileI
 */
return static function($frontend) {
    return new class($frontend) extends Flood\Canal\Feature\File implements Flood\Canal\Feature\FileI {
        protected $access;

        public function __construct($frontend) {
            parent::__construct($frontend);

            $this->access = new User\AccessControl();

            $this->name('user');

            $this->task('install', [$this, 'install']);
            $this->task('uninstall', [$this, 'uninstall']);
            EventDispatcher::on('core.init', [$this, 'init']);
            EventDispatcher::on('core.console.init', [$this, 'initConsole']);
            EventDispatcher::on('feature.user.setActive', [$this, 'setActive']);
            EventDispatcher::on('feature.user.allowed', [$this, 'allowed']);
        }

        public function install() {
            User\User::activate();
            User\Token::activate();
            User\RightManager::activate();
        }

        public function uninstall() {
            User\User::deactivate();
            User\Token::deactivate();
            User\RightManager::deactivate();
        }

        public function setActive() {
            $this->access->setActiveUser();
        }

        /**
         * @param array $payload with id, value and is_allowed, where last may contain the default which is just for non-active feature triggeres, will get updated with true value
         *
         * @return array with updated `is_allowed` when `id` and `value` are set
         */
        public function allowed($payload) {
            if(isset($payload['id']) && isset($payload['value'])) {
                $payload['is_allowed'] = $this->access->allowed($payload['id'], $payload['value']);
            }

            return $payload;
        }

        public function init() {
            $route = require __DIR__ . '/_routeApi.php';
            $route($this->frontend->route);

            $this->access->active = true;
            $this->access->right_manager = new User\RightManager(new User\User(), new User\Token());
        }

        public function initConsole(\Flood\Canal\Console $console) {
            $console::addScope('user', static function() {
                return new User\Console\User();
            });
            $console::addScope('user.token', static function() {
                return new User\Console\UserToken();
            });

            return $console;
        }
    };
};