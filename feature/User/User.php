<?php

namespace Flood\Canal\Feature\User;

use Flood\Canal\Storage\Storage;

class User {
    protected $storage;

    public function __construct() {
        $this->storage = new Storage;
    }

    public static function activate() {
        return (new Storage)->create('user');
    }

    public static function deactivate() {
        return (new Storage)->delete('user');
    }

    /**
     * @param $name
     * @param $pass
     *
     * @return bool
     */
    public function create($name, $pass) {
        $passwd = new Pass();
        return $this->storage->create('user', $name, ['pass' => $passwd->hash($pass)]);
    }

    /**
     * @param $name
     * @param $pass
     *
     * @return bool
     */
    public function updatePass($name, $pass) {
        $passwd = new Pass();
        return $this->storage->update('user', $name, ['pass' => $passwd->hash($pass)]);
    }

    /**
     * @param $name
     * @param $pass
     *
     * @return bool
     */
    public function verify($name, $pass) {
        $passwd = new Pass();
        $user = $this->storage->get('user', $name);
        if(isset($user['pass'])) {
            return $passwd->verify($pass, $user['pass']);
        } else {
            return false;
        }
    }

    public function get($name = null) {
        if(null === $name) {
            return $this->storage->get('user');
        } else {
            return $this->storage->get('user', $name);
        }
    }

    public function exists($name) {
        return $this->storage->has('user', $name);
    }

    public function delete($name) {
        return $this->storage->delete('user', $name);
    }
}