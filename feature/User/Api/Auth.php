<?php

namespace Flood\Canal\Feature\User\Api;

use Flood\Canal\Input\Receive;
// this api controller must extend ApiBase and not ApiController as it must not be checked if anyone has rights, there could not be checked if someone is allowed to call the login api before he is logged in
use Flood\Canal\Controller\ApiBase;
use Flood\Canal\Feature\Api\ApiController;

class Auth extends ApiBase {
    protected $receive;

    /**
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct(\Flood\Canal\Frontend $frontend) {
        parent::__construct($frontend, [
            'origin_allowed' => ApiController::$origin_allowed,
            'header_allowed' => ApiController::$header_allowed,
            'header_expose'  => ApiController::$header_expose,
        ]);

        $this->receive = new Receive();
    }

    /**
     * Handles Login Verification
     */
    public function handleVerify() {
        $user = $this->receive->get('username', FILTER_SANITIZE_STRING);
        $pass = $this->receive->get('password', FILTER_SANITIZE_STRING);

        if(false !== $user && false !== $pass) {
            $u = new \Flood\Canal\Feature\User\User();
            $user = trim($user);
            $pass = trim($pass);
            if($u->verify(trim($user), trim($pass))) {

                if(\Flood\Canal\Feature\User\Token::$jwt_use) {
                    // JWT
                    $token = \Flood\Canal\Feature\User\Token::newJwt($user);
                    if($token) {
                        $this->resp_data = $token;
                    }
                } else {
                    // custom easy token + signature
                    $token = new \Flood\Canal\Feature\User\Token();
                    if($token_info = $token->new($user)) {
                        $this->resp_data = $token_info;
                    }
                }

            }
        }
        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'not-valid'];
        }

        $this->respondJson();
    }

    public function handleLogout() {
        if(\Flood\Canal\Feature\User\Token::$jwt_use) {
            // JWT
            // nothing to do, token is automatically invalid and must be deleted from client not server
            $this->resp_data = ['success' => true];
        } else {
            // custom easy token + signature
            $token = filter_input(INPUT_SERVER, 'HTTP_X_AUTH_TOKEN', FILTER_SANITIZE_STRING);
            $signature = filter_input(INPUT_SERVER, 'HTTP_X_AUTH_SIGNATURE', FILTER_SANITIZE_STRING);
            if(false !== $token && false !== $signature) {
                $t = new \Flood\Canal\Feature\User\Token();
                if($t->verify($token, $signature)) {
                    if($t->delete($token)) {
                        $this->resp_data = ['success' => true];
                    }
                }
            }
        }

        if(empty($this->resp_data)) {
            $this->resp_data = ['error' => 'not-valid'];
        }

        $this->respondJson();
    }
}