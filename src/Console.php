<?php

namespace Flood\Canal;

use Flood\Captn\EventDispatcher;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;

function writeln($msg) {
    echo $msg . "\r\n";
}

class Console {

    /**
     * @var \Symfony\Component\Console\Input\ArgvInput controls and stores the arguments and which are allowed in general
     */
    static $input;

    /**
     * @var string what environment, `dev` is reserved for development and `prod` for production runtime
     */
    static $env;

    /**
     * @var bool
     */
    static $debug;

    /**
     * @var bool
     */
    static $silence;

    /**
     * @var \Flood\Canal\Frontend must be set from the execution layer
     */
    static $frontend;

    /**
     * @var array with `scope` => classname, stores the action scopes to class mapping, is added e.g. during event core.console.init
     */
    static protected $scopes = [];

    /**
     * bootstrap's the console with default canal parameters
     */
    public function boot() {
        static::$input = new ArgvInput();
        static::$input->bind(new InputDefinition([
            new InputArgument('scope', InputArgument::OPTIONAL),
            new InputArgument('task', InputArgument::OPTIONAL),
            new InputArgument('something', InputArgument::OPTIONAL),
            new InputOption('debug', 'd', InputOption::VALUE_NONE),
            new InputOption('silence', 's', InputOption::VALUE_NONE),
            new InputOption('a', 'a', InputOption::VALUE_REQUIRED),
            new InputOption('a0', 'a0', InputOption::VALUE_REQUIRED),
            new InputOption('a1', 'a1', InputOption::VALUE_REQUIRED),
            new InputOption('a2', 'a2', InputOption::VALUE_REQUIRED),
            new InputOption('a3', 'a3', InputOption::VALUE_REQUIRED),
            new InputOption('a4', 'a4', InputOption::VALUE_REQUIRED),
            new InputOption('a5', 'a5', InputOption::VALUE_REQUIRED),
            new InputOption('a6', 'a6', InputOption::VALUE_REQUIRED),
            new InputOption('a7', 'a7', InputOption::VALUE_REQUIRED),
            new InputOption('a8', 'a8', InputOption::VALUE_REQUIRED),
            new InputOption('a9', 'a9', InputOption::VALUE_REQUIRED),
        ]));

        static::$env = static::$input->getParameterOption(['--env', '-e'], 'dev');

        static::$debug = static::$input->hasParameterOption(['--debug', '-d']);

        static::$silence = static::$input->hasParameterOption(['--silence', '-s']);

        if(!static::$silence) {
            writeln('Flood\\Canal: cli running');
        }

        if(!static::$input->getArgument('scope')) {
            if(static::$debug) {
                writeln('Error: Argument `scope` is missing.');
                writeln('abort.');
            }
            if(!static::$silence && !static::$debug) {
                writeln('Aborted.');
            }
            exit(2);
        }

        if(!static::$input->getArgument('task')) {
            if(static::$debug) {
                writeln('Error: Argument `task` is missing.');
                writeln('abort.');
            }
            if(!static::$silence && !static::$debug) {
                writeln('Aborted.');
            }
            exit(2);
        }

        /*if(!static::$input->getArgument('something')) {
            if(static::$debug) {
                writeln('Error: Argument `something` is missing.');
                writeln('abort.');
            }
            if(!static::$silence && !static::$debug) {
                writeln('Aborted.');
            }
            exit(2);
        }*/
    }

    public function init() {
        EventDispatcher::trigger('core.console.init', $this);
    }

    /**
     * @param string   $scope
     * @param callable $callable must return an object on which the tasks get resolved
     */
    public static function addScope($scope, $callable) {
        static::$scopes[$scope] = $callable;
    }

    /**
     * makes the execution command from the cli command
     *
     * @param string      $scope
     * @param string      $task
     * @param string|null $something
     *
     * @throws
     *
     * @example `cache del all` would be `Flood\Canal\Console\Action\Cache->del('all');`
     */
    public function exec($scope, $task, $something = null) {
        if(isset(static::$scopes[$scope])) {
            // must return some object
            $scope = call_user_func(static::$scopes[$scope]);
            if(!is_object($scope)) {
                throw new \Exception('Console: registered scope `' . $scope . '` is not a object');
            }
            // build arguments array with first value of array `something` and all other consisting of existing `-a` `-a1` to `-a10` options
            $arguments = [];
            if($something) {
                $arguments[] = $something;
            }
            for($i = -1; $i < 10; $i++) {
                if(Console::$input->hasOption('a' . (0 <= $i ? $i : ''))) {
                    $arguments[] = Console::$input->getOption('a' . (0 <= $i ? $i : ''));
                } else {
                    break;
                }
            }
            call_user_func_array([$scope, $task], $arguments);
        } else {
            error_log('CLI Scope not found: ' + $scope);
        }
    }
}