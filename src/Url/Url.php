<?php

namespace Flood\Canal\Url;

/**
 * Class Url
 *
 * @package Flood\Canal\Url
 */
class Url {

    /**
     * Checks if given path has a slash at the beginning or true
     *
     * @param string $path
     *
     * @return bool false when nowhere a slash, true when at begin or end
     */
    public function hasSlash($path) {
        $return_val = false;
        if($this->hasSlashBeginning($path) || $this->hasSlashTrailing($path)) {
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * Checks if given path has a slash at the beginning or true
     *
     * @param string $path
     *
     * @return bool false when nowhere a slash, true when at begin or end
     */
    public function hasSlashBeginning($path) {
        $return_val = false;
        $path = trim($path);
        if('/' === substr($path, 0, 1)) {
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * Checks if given path has a slash at the beginning or true
     *
     * @param string $path
     *
     * @return bool false when nowhere a slash, true when at begin or end
     */
    public function hasSlashTrailing($path) {
        $return_val = false;
        $path = trim($path);
        if('/' === substr($path, strlen($path) - 1)) {
            $return_val = true;
        }

        return $return_val;
    }

    /**
     * Strips beginning and end slashes from a string, trims string before removing /
     *
     * @param $string
     *
     * @return string trimmed and / and \ removed from begin and end
     */
    public function stripSlash($string) {
        return $this->stripSlashBeginning($this->stripSlashTrailing($string));
    }

    /**
     * Strips beginning slash from a string, trims string before removing /
     *
     * @param $string
     *
     * @return string trimmed and / and \ removed from begin
     */
    public function stripSlashBeginning($string) {
        $string = trim($string);
        if('\\' === substr($string, 0, 1) || '/' === substr($string, 0, 1)) {
            $string = substr($string, 1);
        }

        return $string;
    }

    /**
     * Strips end slash from a string, trims string before removing /
     *
     * @param $string
     *
     * @return string trimmed and / and \ removed from end
     */
    public function stripSlashTrailing($string) {
        $string = trim($string);
        if('\\' === substr($string, strlen($string) - 1) || '/' === substr($string, strlen($string) - 1)) {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return $string;
    }

    /**
     * Adds a trailing slash to the end of the given path (string) or removes it when not wanted
     *
     * @param string $path
     * @param bool   $trailing_slash true adds a / when not already at the end, false removes a trailing slash if in $uri
     *
     * @return string $uri with slash at end, or not
     */
    public function slashTrailing($path, $trailing_slash = true) {
        $return_val = $path;
        if(true === $trailing_slash) {
            if(!$this->hasSlashTrailing($path)) {
                // When the last char in the uri is not a / but wanted, add it
                $return_val .= '/';
            }
        } else if(false === $trailing_slash) {
            if($this->hasSlashTrailing($path)) {
                // When the last char in the uri is a / but not wanted, removes it
                $return_val = $this->stripSlashTrailing($path);
            }
        }

        return $return_val;
    }

    /**
     * Redirects to a uri with a header code, and adds when wanted additional headers
     *
     * @todo implement additional_header
     *
     * @param      $goto
     * @param int  $header_code
     * @param null $additional_header pre headers are added before header_code and post after ['pre' => [], 'post' => []]
     */
    public function redirect($goto, $header_code = 301, $additional_header = null) {
        static::addStatusHeader($header_code);

        header('Location: ' . $goto);
        exit();
    }

    /**
     * @param int|number $code
     */
    public static function addStatusHeader($code) {
        $header_code_msg = $_SERVER['SERVER_PROTOCOL'] . ' ';

        $header_code_list = [
            /* Successful Operation */
            '200' => 'OK',
            '205' => 'Reset Content',
            '206' => 'Partial Content',
            '207' => 'Multi-Status',

            /* Redirection */
            '300' => 'Multiple Choices',
            '301' => 'Moved Permanently',
            '302' => 'Found',
            '307' => 'Temporary Redirect',
            '308' => 'Permanent Redirect',

            /*  Client-Error */
            '401' => 'Unauthorized',
            '403' => 'Forbidden',
            '404' => 'Not Found',
            '408' => 'Request Time-out',
            '410' => 'Gone',
            '418' => 'I\'m a teapot',
            '421' => 'There are too many connections from your internet address',
            '423' => 'Locked',
            '424' => 'Failed Dependency',
            '429' => 'Too Many Requests',
            '451' => 'Unavailable For Legal Reasons',

            /* Server-Error */
            '503' => 'Service Unavailable',
        ];

        $header_code_msg .= $code;
        if(array_key_exists($code, $header_code_list)) {
            $header_code_msg .= ' ' . $header_code_list[$code];
        }

        header($header_code_msg);
    }
}