<?php

namespace Flood\Canal\Feature;

interface FileI {
    public function name($name = null);

    public function task($name, $callable);

    public function taskList();
}