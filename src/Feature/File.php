<?php

namespace Flood\Canal\Feature;

class File {
    protected $frontend;

    private $name = '';
    private $task_list = [];

    /**
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct($frontend) {
        $this->frontend = $frontend;
    }

    /**
     * Sets or gets the `name` of the feature
     *
     * @param null $name
     *
     * @return string
     */
    public function name($name = null) {
        if(null !== $name) {
            $this->name = $name;
        }
        return $this->name;
    }

    /**
     * Sets a `hook` of the feature, attaching to execution tasks
     *
     * @param string   $name
     * @param callable $callable
     */
    public function task($name, $callable) {
        if(null !== $name && null !== $callable) {
            $this->task_list[$name] = $callable;
        }
    }

    /**
     * @return array list of currently existing hooks for this feature
     */
    public function taskList() {
        return array_keys($this->task_list);
    }
}