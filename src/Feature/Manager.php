<?php

namespace Flood\Canal\Feature;

class Manager {
    protected $list_feature = [];
    protected $list_task = [];
    protected $frontend;

    public function __construct($frontend) {
        $this->frontend = $frontend;
    }

    /**
     * @param \Closure|\Flood\Canal\Feature\FileI $feature
     * @throws \Exception
     */
    public function add($feature) {
        if(is_object($feature) && ($feature instanceof \Closure)) {
            $feature = call_user_func($feature, $this->frontend);
        }

        if(
            !is_subclass_of($feature, 'Flood\Canal\Feature\FileI') ||
            !is_subclass_of($feature, 'Flood\Canal\Feature\File')
        ) {
            throw new \Exception('FeatureManager: feature file doesn\'t implement Feature\File or its interface Feature\FileI');
        }

        if(!is_object($feature)) {
            echo 'Wrong Type';
            //var_dump($feature);
            exit();
        }

        $this->list_feature[$feature->name()] = $feature;
    }

    /**
     * @param string $name the name of the feature
     * @param string $task id of the task that should be executed
     * @param array  $data
     *
     * @return mixed
     */
    public function on($name, $task, $data = []) {
        if(isset($this->list_feature[$name])) {
            /**
             * @var \Flood\Canal\Feature\FileI $feature
             */
            $feature = $this->list_feature[$name];
            $task_list = $feature->taskList();
            if(isset($task_list[$task])) {
                return call_user_func_array($task_list[$task], $data);
            }
        }

        return null;
    }
}