<?php

namespace Flood\Canal\Controller;

use Flood\Canal\Feature\Api\ApiController;

/**
 * Route Factory and Api Controller in one part.
 *
 * Easily setup routes to APIs with small OPTIONS controller included.
 */
class ApiRoute {
    protected $id;
    protected $path;
    protected $defaults;
    protected $requirements;
    /**
     * @var \Flood\Canal\Route\Routing
     */
    protected $router;
    protected $routes = [];
    protected $options_set = false;

    public static $path_base = '';

    public function __construct($id, $path, $defaults = [], $requirements = []) {
        $this->id = $id;
        $this->path = $path;
        $this->defaults = $defaults;
        $this->requirements = $requirements;
    }

    public function setRouter($router) {
        $this->router = $router;

        return $this;
    }

    protected function createRoute($id, $controller, $utf8, $methods) {
        $this->router->add($id, [
            'path'         => static::$path_base . $this->path,
            'controller'   => $controller,
            'options'      => ($utf8 ? ['utf8' => true] : []),
            'methods'      => $methods,
            'defaults'     => $this->defaults,
            'requirements' => $this->requirements,
        ]);

        if(false === $this->options_set && !in_array('OPTIONS', $methods)) {
            $this->options($utf8);
            $this->options_set = true;
        }

        return $this;
    }

    protected function options($utf8 = false) {
        $methods = [
            'OPTIONS',
        ];
        $this->createRoute(
            $this->id . '--options',
            static function($frontend) {
                // todo: add setOptionsController for using different options

                // a simple 200er return with all headers defined in `ApiBase` but none logic/validation which would be `Api`
                $controller = new ApiBase($frontend, [
                    'origin_allowed' => ApiController::$origin_allowed,
                    'header_allowed' => ApiController::$header_allowed,
                    'header_expose'  => ApiController::$header_expose,
                ]);
                $controller->setResponse('');
                $controller->respond();
                exit(0);
            },
            $utf8,
            $methods
        );
    }

    public function get($controller, $utf8 = false) {
        $methods = [
            'GET',
        ];
        $this->createRoute(
            $this->id . '--get',
            $controller,
            $utf8,
            $methods
        );

        return $this;
    }

    public function post($controller, $utf8 = false) {
        $methods = [
            'POST',
        ];
        $this->createRoute(
            $this->id . '--post',
            $controller,
            $utf8,
            $methods
        );

        return $this;
    }

    public function put($controller, $utf8 = false) {
        $methods = [
            'PUT',
        ];
        $this->createRoute(
            $this->id . '--put',
            $controller,
            $utf8,
            $methods
        );

        return $this;
    }

    public function delete($controller, $utf8 = false) {
        $methods = [
            'DELETE',
        ];
        $this->createRoute(
            $this->id . '--delete',
            $controller,
            $utf8,
            $methods
        );

        return $this;
    }
}