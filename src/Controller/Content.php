<?php

namespace Flood\Canal\Controller;

use Flood\Captn\EventDispatcher;
use Flood\Component\Func\Array_;

class Content extends Base {
    /**
     * @var \Flood\Canal\Service\Md\Parser
     */
    public static $md;

    /**
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);

        $active_article = $this->frontend->content->match($this->frontend->route->request, $this->frontend->match);


        $tpl_data =& $this->tpl_data;
        $renderer = static function($template, $type, $data = null) use (&$tpl_data) {
            if(null !== $data) {
                // todo: should be too time consuming on many blocks in ArticleTree
                $data = Array_::merge_recursive_distinct($tpl_data, $data);
            } else {
                $data = $tpl_data;
            }
            $return = '';
            $result = EventDispatcher::trigger('view.render.' . $type, ['tpl' => $template, 'data' => $data]);
            if(isset($result['rendered'])) {
                $return = $result['rendered'];
            }
            return $return;
        };

        $active_article->provideRenderEnv(
            static function($content_raw, $data = null) use ($renderer) {
                // renderer text
                return $renderer($content_raw, 'string', $data);
            },
            static function($file, $data = null) use ($renderer) {
                // renderer file
                return $renderer($file, 'file', $data);
            },
            function($template_parsed) {
                // parser for MarkDown
                return static::$md->text($template_parsed);
            }
        );
    }

    public function assignDefault() {
        parent::assignDefault();

        $this->assignByRef('content', $this->frontend->content);

        if(!empty($this->frontend->content->getActiveArticle()->meta('meta'))) {
            $this->assign('meta', $this->frontend->content->getActiveArticle()->meta('meta'));
        }
        if(!empty($this->frontend->content->getActiveArticle()->meta('head'))) {
            $this->assign('head', $this->frontend->content->getActiveArticle()->meta('head'));
        }
    }
}