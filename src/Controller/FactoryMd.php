<?php

namespace Flood\Canal\Controller;

use Flood\Canal\Url\Url;
use Flood\Component\PerformanceMonitor\Monitor;

/**
 * Class FactoryMd
 *
 * @package Flood\Canal\Controller
 * @todo    rework completely
 */
class FactoryMd extends Base {

    /**
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct($frontend) {
        parent::__construct($frontend);
        $this->assign('head', [
            'title'  => ucwords(str_replace(['\\', '/', '-', '_', '.',], [' ', ' ', ' ', ' ', ' ',], $this->context->getPathInfo()) . ' | PaintTheWeb'),
            'author' => 'Michael Becker',
        ]);
        if($this->frontend->content->index->existSection($this->frontend->match['_route'])) {
            if($this->frontend->content->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])) {
                if($this->frontend->content->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->meta('meta')) {
                    $this->assign('meta', $this->frontend->content->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->meta('meta'));
                }
            }
        }
    }

    /**
     * @param string $template path to template file, when null used FactoryMd.twig
     * @param string $file     relative file of content .md withing /content/
     *
     * @return string
     */
    public function respond($template = null, $file = null) {
        $md_content = '<h2>404</h2><p>Not Found: ';

        Monitor::i()->startProfile('FactoryMd-response');

        // Try to fetch automatically a file in the folder /content that is named like the path and parse it from MarkDown to HTML
        if(null === $file) {
            $file = __DIR__ . '/../content/' . $this->context->getPathInfo() . '.md';
        } else {
            $file = __DIR__ . '/../content/' . $file;
        }
        if(false !== ($file_content = $this->md->textFile($file))) {
            $md_content = $file_content;
        } else {
            Url::addStatusHeader('404');
            $md_content .= 'content' . $this->context->getPathInfo() . '.md</p>';
        }

        Monitor::i()->endProfile('FactoryMd-response');

        $this->assign('md_content', $md_content);
        if($this->frontend->content->index->existSection($this->frontend->match['_route'])) {
            if($this->frontend->content->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])) {
                if($this->frontend->content->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->meta('sendHeader')) {
                    $this->addHeader($this->frontend->content->getSection($this->frontend->match['_route'])->getArticle($this->frontend->match['article'])->meta('sendHeader'), true);
                }
            }
        }
        if(null !== $template) {
            return $this->render($template);
        } else {
            return $this->render('FactoryMd.twig');
        }
    }
}