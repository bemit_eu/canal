<?php

namespace Flood\Canal\Controller;

use Flood\Captn\EventDispatcher;

class Base {

    public $path = [];

    /**
     * Data that should be inserted the template
     *
     * @var array
     */
    protected $tpl_data = [];

    /**
     * @var \Flood\Canal\Frontend
     */
    protected $frontend;

    protected $path_template = null;

    protected $header = [];

    /**
     * @param \Flood\Canal\Frontend $frontend
     */
    public function __construct($frontend) {
        $this->frontend = $frontend;

        // todo: move to some feature code
        $this->path['asset'] = '/data/out/';
    }

    public function addHeader($header, $as_array = false) {
        if($as_array) {
            $this->header = array_merge($this->header, $header);
        } else {
            $this->header[] = $header;
        }
    }

    /**
     * Same like render() but sends headers like 404
     *
     * @param $template
     * @param $assign_default
     *
     * @return string
     */
    public function respond($template = null, $assign_default = true) {
        $this->sendHeader();

        return $this->render($template, $assign_default);
    }

    public function sendHeader() {
        foreach($this->header as $h) {
            header($h, true);
        }
    }

    /**
     * @param string $template relative path to template file, from Frontend->path_view on
     * @param        $assign_default
     *
     * @return string
     */
    public function render($template = null, $assign_default = true) {
        if($assign_default) {
            $this->assignDefault();
        }
        if(null === $template) {
            $template = $this->path_template;
        }

        $return = '';
        $result = EventDispatcher::trigger('view.render.file', ['tpl' => $template, 'data' => $this->tpl_data]);
        if(isset($result['rendered'])) {
            $return = $result['rendered'];
        }

        return $return;
    }

    public function assignDefault() {
        /**
         * @var \Symfony\Component\Routing\RequestContext
         */
        $this->assignByRef('requestContext', $this->frontend->route->request_context);
        $this->assignByRef('frontend', $this->frontend);

        $this->assignByRef('match', $this->frontend->match);
        $this->assign('url', [
            'asset'    => $this->frontend->route->request_context->getScheme() . '://' . $this->frontend->route->request_context->getHost() . ($this->frontend->route->request_context->getHttpPort() !== 80 ? ':' . $this->frontend->route->request_context->getHttpPort() : '') . $this->path['asset'],
            'home'     => $this->frontend->url_generator->generate('home'),
            'generate' => $this->frontend->url_generator,
        ]);
    }

    public function assignByRef($key, &$val) {
        $this->tpl_data[$key] = &$val;
    }

    public function assign($key, $val) {
        $this->tpl_data[$key] = $val;
    }
}