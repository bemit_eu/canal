<?php

namespace Flood\Canal\Controller;

class ApiBase extends Base {
    protected $error = false;

    /**
     * @var array
     */
    protected $resp_data = [];

    /**
     *
     * @param \Flood\Canal\Frontend $frontend
     * @param array                 $config
     */
    public function __construct(\Flood\Canal\Frontend $frontend, $config) {
        parent::__construct($frontend);

        if(isset($config['origin_allowed'])) {
            // multiple origins are allowed through checking if the current one is an allowed one, that ONE origin is set as allowed origin

            // get accessing origin from header
            $current_origin = filter_input(INPUT_SERVER, 'HTTP_ORIGIN', FILTER_SANITIZE_URL);

            // multi-origin check
            if(!empty($current_origin) && in_array($current_origin, $config['origin_allowed'])) {
                // when current is allowed, set header
                $this->addHeader('Access-Control-Allow-Origin: ' . $current_origin);
            }
            $this->addHeader('Vary: Origin');
        }

        $this->addHeader('Access-Control-Allow-Credentials: true');
        $this->addHeader('Access-Control-Allow-Methods: OPTIONS, POST, GET, DELETE, PUT');

        // default: 10min. caching of allow methods/allow headers; is max. amount for chromium
        $this->addHeader('Access-Control-Max-Age: 600');

        if(isset($config['header_allowed'])) {
            $this->addHeader('Access-Control-Allow-Headers: ' . implode(', ', $config['header_allowed']));
        }

        if(isset($config['header_expose'])) {
            $this->addHeader('Access-Control-Expose-Headers: ' . implode(', ', $config['header_expose']));
        }
    }

    public function setResponse($resp) {
        $this->resp_data = $resp;
    }

    public function respondJson() {
        $this->addHeader('Content-Type: application/json');
        $this->resp_data = json_encode($this->resp_data);
        $this->respond();
    }

    public function respond($template = null, $assign_default = true) {
        $this->sendHeader();

        if(true !== $this->error) {
            echo $this->resp_data;
        }
    }
}