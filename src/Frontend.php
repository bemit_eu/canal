<?php

namespace Flood\Canal;

use Flood\Canal\Url\Url;
use Flood\Captn\EventDispatcher;

/**
 * Class Frontend
 *
 * @package Flood\Canal
 */
class Frontend {

    //
    // Debug Vars
    //

    /**
     * @var bool
     */
    public $debug = false;

    //
    // Path Vars
    //

    /**
     * @var string the folder of temporary files, must be set from flow.php
     */
    public $path_tmp = '';

    /**
     * @var string contains the json-db folders
     */
    public $path_data = '';

    /**
     * @var array the folder of template/view files, must be set from flow.php
     * @deprecated direct access will be dismissed in the future, use addViewDir() instead
     */
    public $path_view = [];

    //
    // URL Vars
    //

    /**
     * @var array
     * @deprecated only applies to direct access, use `route` with Routing instead
     */
    public $route_list = [];

    /**
     * @var \Flood\Canal\Route\Routing
     */
    public $route;

    /**
     * @var string will be used to only allow routes from this host, when not another is defined within routes options. ['host'=>'val']
     *             if this property is empty, it will not be used
     *             it is recommended to restrict the host access.
     */
    public $host = '';

    /**
     * @var bool
     */
    public $ssl = true;

    /**
     * @var \Flood\Canal\Route\UrlGenerator
     */
    public $url_generator;

    /**
     * @var array|bool contains the result of the match
     */
    public $match = false;

    //
    // Content Handler
    //

    /**
     * @var \Flood\Canal\Feature\Content\Content
     */
    public $content;

    //
    // Feature
    //

    /**
     * @var \Flood\Canal\Feature\Manager
     */
    public $feature;

    /**
     * @param bool $debug backward compatibility
     */
    public function __construct($debug = false) {
        $this->debug = $debug;
        $this->feature = new Feature\Manager($this);
    }

    public function addViewDir($path, $namespace = null) {
        if(null === $namespace) {
            $this->path_view[] = $path;
        } else {
            $this->path_view[$path] = $namespace;
        }
        return $this;
    }

    public function init() {
        EventDispatcher::trigger('core.init');

        EventDispatcher::trigger('feature.user.setActive');
    }

    /**
     * Executes render and displays the page
     */
    public function display() {
        echo $this->render();
    }

    protected function routeMatch() {
        $this->match = EventDispatcher::trigger('core.route.match');
        if(isset($this->match['error'])) {
            Url::addStatusHeader($this->match['error']['code']);
            echo json_encode('Frontend.routeMatch Error: ' . $this->match['error']['type']);
            exit();
        }
    }

    /**
     *
     * @return mixed
     */
    public function render() {

        // todo: for route emulation through CLI, routeMatch must be split from render and a little bit more
        $this->routeMatch();

        $allowed = EventDispatcher::trigger('feature.user.allowed', [
            'id'         => 'CONTROLLER_CALL',
            'value'      => $this->match['_route'],
            'is_allowed' => true,
        ]);

        if($allowed['is_allowed']) {
            // todo: refactor to feature/flood-captn piping event
            return call_user_func_array($this->match['_controller'], [$this]);
        } else {
            // todo: add custom error page for 403
            Url::addStatusHeader(403);
            echo json_encode('Access to route not allowed.');
            exit();
        }
    }
}