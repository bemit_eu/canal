<?php

namespace Flood\Canal\Route;

use Flood\Canal\Url\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Routing {
    /**
     * @var bool
     */
    public $debug = false;

    /**
     * @var array
     */
    public $route_list = [];

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    public $request;

    /**
     * @var \Symfony\Component\Routing\RequestContext
     */
    public $request_context;

    /**
     * @var \Symfony\Component\Routing\Generator\UrlGenerator
     */
    public $url_generator;

    /**
     * @var \Symfony\Component\Routing\RouteCollection
     */
    public $routes;

    /**
     * @var string will be used to only allow routes from this host, when not another is defined within routes options. ['host'=>'val']
     *             if this property is empty, it will not be used
     *             it is recommended to restrict the host access.
     */
    public $host = '';

    /**
     * @var bool
     */
    public $ssl = true;

    public function __construct(&$host, &$ssl, &$debug) {
        $this->host = &$host;
        $this->ssl = &$ssl;
        $this->debug = &$debug;
        $this->routes = new RouteCollection();
    }

    /**
     * Automatically find current port depending on active scheme
     *
     * @todo move to route_context
     */
    public function getPort() {
        $scheme = 'get' . ucfirst($this->request_context->getScheme()) . 'Port';
        return $this->request_context->$scheme();
    }

    public function addList($list) {
        foreach($list as $id => $r) {
            $this->add($id, $r);
        }
    }

    public function add($id, $route) {
        if(isset($route['path'])) {

            if($this->debug) {
                echo 'Added Response: ' . "\r\n" .
                    'name/id:    ' . $id . "\r\n" .
                    'path:       ' . (isset($route['path']) && !empty($route['path']) ? $route['path'] : '""') . "\r\n" .
                    'host:       ' . (isset($route['host']) && !empty($route['host']) ? $route['host'] : '""') . "\r\n" .
                    'methods:    ' . (isset($route['methods']) && !empty($route['methods']) ? json_encode($route['methods']) : '""') . "\r\n" .
                    'schemes:    ' . ($this->ssl ? 'HTTPS' : 'HTTP') . "\r\n" . "\r\n";
            }

            $this->routes->add(
                $id,
                new Route(
                    $route['path'], // path
                    (isset($route['defaults']) ? array_merge(['_controller' => $route['controller']], $route['defaults']) : ['_controller' => $route['controller']]), // parameters
                    (isset($route['requirements']) ? $route['requirements'] : []), // requirement
                    (isset($route['options']) ? $route['options'] : []), // options
                    (isset($route['host']) ? $route['host'] : (!empty($this->host) ? $this->host : '')), // options, // host
                    [($this->ssl ? 'HTTPS' : 'HTTP')], // schemes
                    (isset($route['methods']) ? $route['methods'] : []) // methods
                ));
        }

        return $this;
    }

    public function match() {
        $this->request = Request::createFromGlobals();
        $this->request_context = new RequestContext('/');
        $this->request_context->fromRequest($this->request);

        // Determine route and execute callback.
        $matcher = new UrlMatcher($this->routes, $this->request_context);

        $this->url_generator = new UrlGenerator($this->routes, $this->request_context);

        if('/' !== $this->request_context->getPathInfo() && '/' === substr($this->request_context->getPathInfo(), strlen($this->request_context->getPathInfo()) - 1)) {
            // redirect on trailing slash
            // but only when the active path is not only a /, like on https://domain.de/
            $uri = new Url();
            $uri->redirect(str_replace($this->request_context->getPathInfo(), rtrim($this->request_context->getPathInfo(), ' /'), $this->request->getRequestUri()));
        }

        try {
            try {
                $match = $matcher->match($this->request_context->getPathInfo());
            } catch(\Symfony\Component\Routing\Exception\MethodNotAllowedException $e) {
                $match = [
                    'error' => [
                        'type' => 'MethodNotAllowedException',
                        'code' => '405',
                    ],
                ];

                error_log('Flood\Canal: Fatal Error in Frontend.render during matching: current method is `' . $this->request_context->getMethod() . '` but only allowed are: `' . json_encode($e->getAllowedMethods()) . '` for `' . $this->request_context->getPathInfo() . '`');
            }
        } catch(\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
            $match = [
                'error' => [
                    'type' => 'ResourceNotFoundException',
                    'code' => '404',
                ],
            ];

            error_log('Flood\Canal: Fatal Error in Frontend.render during matching: resource for request with method `' . $this->request_context->getMethod() . '` and path `' . $this->request_context->getPathInfo() . '`');
        }

        return $match;
    }
}