<?php

namespace Flood\Canal\Route;

use Symfony\Component\Routing\Generator\UrlGenerator as SymfonyUrlGenerator;

class UrlGenerator extends SymfonyUrlGenerator {

    /**
     * @param       $name
     * @param array $parameters
     * @param int   $referenceType
     *
     * @return string
     */
    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_URL) {
        return parent::generate($name, $parameters, $referenceType);
    }
}