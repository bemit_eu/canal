<?php

namespace Flood\Canal\Service\Md;

use Symfony\Component\DomCrawler\Crawler;

class Parser {

    /**
     * @var \Parsedown
     */
    public $parsedown;

    protected $crawler;

    /**
     * Markdown constructor.
     */
    public function __construct() {
        $this->parsedown = new \Parsedown();
        $this->crawler = new Crawler();
    }

    /**
     * @param $path
     *
     * @return bool|string
     */
    public function textFile($path) {
        if(false !== ($file_content = file_get_contents($path))) {
            return $this->text($file_content);
        } else {
            return false;
        }
    }

    /**
     * @param $text
     *
     * @return string
     */
    public function text($text) {
        return $this->parsedown->text($text);
    }

    /**
     * Could be used for generating a index of table, when not done through JS
     *
     * @param $text
     */
    public function crawl($text) {
        $this->crawler->addContent($text);
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH1() {
        return $this->crawler->filter('h1');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH2() {
        return $this->crawler->filter('h2');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH3() {
        return $this->crawler->filter('h3');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH4() {
        return $this->crawler->filter('h4');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH5() {
        return $this->crawler->filter('h5');
    }

    /**
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getH6() {
        return $this->crawler->filter('h6');
    }
}