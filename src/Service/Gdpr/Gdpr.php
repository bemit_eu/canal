<?php

namespace Flood\Canal\Service\Gdpr;

/**
 * Data Handler Class for Frontend Component GDPR
 *
 * Social embeds and so on
 *
 * @package Flood\Canal\Gdpr
 */
class Gdpr {
    protected $global = [];
    protected $element_list = [];

    public function __construct($setting) {
        if(isset($setting['global'])) {
            $this->global = $setting['global'];
        }
        if(isset($setting['element_list'])) {
            $this->element_list = $setting['element_list'];
        }
    }

    /**
     * Merges global with the tpl data when needed
     *
     * @param $family
     * @param $tpl_gdpr
     *
     * @return array
     */
    public function computeGlobal($family, $tpl_gdpr) {
        if(isset($this->global[$family]) && isset($tpl_gdpr) && is_array($tpl_gdpr)) {
            return \Flood\Component\Func\Array_::merge_recursive_distinct($this->global[$family], $tpl_gdpr);
        } else {
            return $tpl_gdpr;
        }
    }
}