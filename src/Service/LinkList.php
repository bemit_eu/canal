<?php

namespace Flood\Canal\Service;


class LinkList {

    /**
     * @var array
     */
    public $list = [];

    public function __construct() {
    }

    /**
     * @param string $name what should be displayed as link
     * @param string $link
     * @param string $target
     * @param array  $opt  will be merged in to the link info
     */
    public function addLink($name, $link, $target = '', $opt = []) {
        $this->list[] = array_merge(['name' => $name, 'link' => $link, 'target' => $target], $opt);
    }

    /**
     * @param string   $label
     * @param LinkList $list
     */
    public function addLinkList($label, LinkList $list) {
        $this->list[] = [
            'label' => $label,
            'list'  => $list->list,
        ];
    }
}