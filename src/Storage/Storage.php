<?php

namespace Flood\Canal\Storage;

class Storage {
    public const STORAGE_JSON = 0;
    public static $storage_handler = [];

    /**
     * @var \Flood\Canal\Storage\Handler
     */
    protected $handler;

    public function __construct($type = 0) {
        if(isset(static::$storage_handler[$type])) {
            $classname = static::$storage_handler[$type];
            $this->handler = new $classname($this);
        }
    }

    public function create($list, $item = null, $data = null) {
        try {
            if(null === $item) {
                return $this->handler->createList($list);
            } else {
                return $this->handler->createItem($list, $item, $data);
            }
        } catch(\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function update($list, $item, $data) {
        try {
            return $this->handler->updateItem($list, $item, $data);
        } catch(\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function has($list, $item = null) {
        if(null === $item) {
            return $this->handler->hasList($list);
        } else {
            return $this->handler->hasItem($list, $item);
        }
    }

    public function get($list, $item = null, $condition = null) {
        if(null === $item) {
            return $this->handler->getList($list);
        } else {
            return $this->handler->getItem($list, $item, $condition);
        }
    }

    public function delete($list, $item = null) {
        try {
            if(null === $item) {
                return $this->handler->deleteList($list);
            } else {
                return $this->handler->deleteItem($list, $item);
            }
        } catch(\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }
}