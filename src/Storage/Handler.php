<?php

namespace Flood\Canal\Storage;


interface Handler {
    public function getList($list);

    public function getItem($list, $item, $condition);

    public function hasList($list);

    public function hasItem($list, $item);

    public function updateItem($list, $item, $data, $condition);

    public function createList($list);

    public function createItem($list, $item, $data);

    public function deleteList($list);

    public function deleteItem($list, $item);
}