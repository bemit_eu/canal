<?php

namespace Flood\Canal\Input;

class Receive {

    protected static $input_data = false;
    protected static $post_data = false;

    public function __construct() {

        if(false === static::$input_data) {
            $input = file_get_contents('php://input');

            if($input) {
                // exists
                static::$input_data = $json = json_decode($input, true);
            }
        }

        if(false === static::$post_data) {
            static::$post_data = true;
        }
    }

    /**
     * Use only for debugging, no included filter
     *
     * @return mixed|array
     */
    public function all() {

        if(static::$input_data) {
            return static::$input_data;
        } else if(static::$post_data) {
            return $_POST;
        }

        return [];
    }

    /**
     * @param     $key
     * @param int $filter
     *
     * @return mixed|null
     */
    public function get($key, $filter = FILTER_DEFAULT) {
        $return_val = null;

        if(static::$input_data && isset(static::$input_data[$key])) {
            $return_val = filter_var(static::$input_data[$key], $filter);
        } else if(static::$post_data && filter_has_var(INPUT_POST, $key)) {
            $return_val = filter_input(INPUT_POST, $key, $filter);
        }

        return $return_val;
    }
}